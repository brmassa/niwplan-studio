namespace AtomicMassa.Tests;

/// <summary>
/// Test Nodes, their hierarchy and Transform operations
/// </summary>
public class NodeTest
{
    private sealed class Node2 : Node;

    private readonly Node nodeMain = new() { Name = "Cube -1" };
    private readonly int nodeLayers = 10;
    private Vector3D<float> positionDefault = new(0, 1, 0);
    private Vector3D<float> rotationDefault = new(45, 0, 0);
    private Vector3D<float> scaleDefault = new(2, 2, 2);

    /// <summary>
    /// Constructor and setup for all tests
    /// </summary>
    public NodeTest()
    {
        var cubeBase = nodeMain;
        for (var layer = 0; layer < nodeLayers; layer++)
        {
            var cube = layer % 2 == 0 ? new Node() : new Node2();
            cube.Name = $"Cube {layer}";
            cube.Transform.Position = positionDefault;
            cube.Transform.Rotation = rotationDefault;
            cube.Transform.Scale = scaleDefault;
            cubeBase.Children.Add(cube);
            cubeBase = cube;
        }
        nodeMain.Initialize(null, null!);
    }

    /// <summary>
    /// Check if it returns the correct number of children
    /// </summary>
    [Fact]
    public void NodeGetChildren()
    {
        var list = Node.GetChildren(nodeMain).ToList();

        Assert.Equal(10, list.Count);
    }

    /// <summary>
    /// Check if it returns the correct number of children with a given type
    /// </summary>
    [Fact]
    public void NodeGetChildrenByType()
    {
        var list = Node.GetChildren<Node2>(nodeMain).ToList();

        Assert.Equal(5, list.Count);
    }

    /// <summary>
    /// Check if it returns the correct number of children with a given type
    /// </summary>
    [Fact]
    public void NodeGetTypeAndChildrenByType()
    {
        var list = Node.GetTypeAndChildren<Node2>(nodeMain.Children).ToList();

        Assert.Equal(5, list.Count);
    }

    /// <summary>
    /// Check the global Transform Rotation from a child Node
    /// </summary>
    [Fact]
    public void GlobalTramsformRotation()
    {
        var node = nodeMain;
        var orientation = nodeMain.Transform.Orientation;

        var orientationDefault = rotationDefault.ToQuaternion();
        var layers = 9;
        for (var i = 0; i < layers; i++)
        {
            node = node.Children[0];
            orientation *= orientationDefault;
        }

        var result =
            rotationDefault
            * (layers > 0 ? new Vector3D<float>(layers, 1, 1) : Vector3D<float>.One);

        Assert.Equal($"Cube {layers - 1}", node.Name);
        Assert.Equal(orientation, node.GlobalTransform.Orientation);

        // only double has precision, so convert to double. Otherwise, floats can be lightly different
        Assert.Equal((double)result.X % 360, node.GlobalTransform.Rotation.X, 3);
    }

    /// <summary>
    /// Check the global Transform Scale from a child Node
    /// </summary>
    [Fact]
    public void GlobalTramsformScale()
    {
        var node = nodeMain;
        var expected = Vector3D<float>.One;

        var layers = 4;
        for (var i = 0; i < layers; i++)
        {
            node = node.Children[0];
            expected *= scaleDefault;
        }

        Assert.Equal($"Cube {layers - 1}", node.Name);
        Assert.Equal(expected, node.GlobalTransform.Scale);
    }

    /// <summary>
    /// Check the global Transform Position from a child Node
    /// </summary>
    [Fact]
    public void GlobalTramsformPosition()
    {
        var node = nodeMain;
        var expected = Mathf.Up;

        var layers = 1;
        for (var i = 0; i < layers; i++)
        {
            node = node.Children[0];
        }

        Assert.Equal($"Cube {layers - 1}", node.Name);
        Assert.Equal(expected, node.GlobalTransform.Position);
    }

    /// <summary>
    /// Check the Transform Position values when adding/removing pareting
    /// </summary>
    [Fact]
    public void GlobalTramsformAddRemoveParent()
    {
        var t1 = new Transform
        {
            Position = positionDefault,
            Rotation = rotationDefault,
            Scale = scaleDefault
        };

        var t2 = new Transform
        {
            Position = positionDefault * 2,
            Rotation = rotationDefault * 2,
            Scale = scaleDefault * 2
        };

        var t3 = new Transform
        {
            Position = positionDefault * 3,
            Rotation = rotationDefault * 3,
            Scale = scaleDefault * 3
        };

        var result2 = t2.AddParent(t1).RemoveParent(t1);
        var result3 = t3.AddParent(t2.AddParent(t1)).RemoveParent(t2.AddParent(t1));

        Assert.Equal((double)t2.Position.X, result2.Position.X, 4);
        Assert.Equal((double)t2.Position.Y, result2.Position.Y, 4);
        Assert.Equal((double)t2.Position.Z, result2.Position.Z, 4);
        Assert.Equal((double)t2.Orientation.X, result2.Orientation.X, 4);
        Assert.Equal((double)t2.Orientation.Y, result2.Orientation.Y, 4);
        Assert.Equal((double)t2.Orientation.Z, result2.Orientation.Z, 4);
        Assert.Equal((double)t2.Scale.X, result2.Scale.X, 4);
        Assert.Equal((double)t2.Scale.Y, result2.Scale.Y, 4);
        Assert.Equal((double)t2.Scale.Z, result2.Scale.Z, 4);

        Assert.Equal((double)t3.Position.X, result3.Position.X, 4);
        Assert.Equal((double)t3.Position.Y, result3.Position.Y, 4);
        Assert.Equal((double)t3.Position.Z, result3.Position.Z, 4);
        Assert.Equal((double)t3.Orientation.X, result3.Orientation.X, 4);
        Assert.Equal((double)t3.Orientation.Y, result3.Orientation.Y, 4);
        Assert.Equal((double)t3.Orientation.Z, result3.Orientation.Z, 4);
        Assert.Equal((double)t3.Scale.X, result3.Scale.X, 4);
        Assert.Equal((double)t3.Scale.Y, result3.Scale.Y, 4);
        Assert.Equal((double)t3.Scale.Z, result3.Scale.Z, 4);
    }
}
