namespace AtomicMassa.Editor.Studio.Panels.Inspector.Editors;

/// <summary>
/// Base class for all "composite" (the ones that might have values inside) property editors
/// </summary>
public abstract class CompositePropertyEditor : UserControl
{
    /// <summary>
    /// Collapsible panel
    /// </summary>
    protected Expander Expander { get; private set; }

    /// <summary>
    /// Content of the panel
    /// </summary>
    protected StackPanel Panel { get; private set; }

    /// <summary>
    /// Header of the panel (similar to a label)
    /// </summary>
    protected TextBlock Header { get; private set; }

    /// <summary>
    /// Create a new composite property editor
    /// </summary>
    protected CompositePropertyEditor(MemberInfo? memberInfo)
    {
        Panel = new StackPanel
        {
            Orientation = Orientation.Vertical,
            Margin = new Thickness(0, 5, 0, 5)
        };

        Header = new TextBlock
        {
            Text = (memberInfo?.Name ?? "").ToReadableName(),
            Width = 100
        };
        Expander = new Expander { Header = Header, Content = Panel };

        Content = Expander;
    }
}
