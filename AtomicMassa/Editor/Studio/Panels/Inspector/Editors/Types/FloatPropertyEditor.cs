namespace AtomicMassa.Editor.Studio.Panels.Inspector.Editors.Types;

/// <summary>
/// float property editor
/// </summary>
/// <inheritdoc/>
[CustomEditor(editorType: typeof(float))]
public class FloatPropertyEditor(MemberInfo memberInfo, object targetObject) : IntegerPropertyEditor<float>(memberInfo, targetObject);

/// <summary>
/// double property editor
/// </summary>
/// <inheritdoc/>
[CustomEditor(editorType: typeof(double))]
public class DoublePropertyEditor(MemberInfo memberInfo, object targetObject) : IntegerPropertyEditor<double>(memberInfo, targetObject);

/// <summary>
/// decimal property editor
/// </summary>
/// <inheritdoc/>
[CustomEditor(editorType: typeof(decimal))]
public class DecimalPropertyEditor(MemberInfo memberInfo, object targetObject) : IntegerPropertyEditor<decimal>(memberInfo, targetObject);
