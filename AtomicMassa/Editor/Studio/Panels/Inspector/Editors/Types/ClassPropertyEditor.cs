namespace AtomicMassa.Editor.Studio.Panels.Inspector.Editors.Types;

/// <summary>
/// Property editor for classes and structs
/// </summary>
public class ClassPropertyEditor : CompositePropertyEditor
{
    /// <inheritdoc/>
    public ClassPropertyEditor(MemberInfo memberInfo, object targetObject)
        : base(memberInfo)
    {
        var value = memberInfo.GetValue(targetObject);
        if (value != null)
        {
            Expander.IsExpanded = GetDefaultExpanded(memberInfo);

            var propertyEditors = PropertyEditorFactory.CreateEditors(value);
            foreach (var editor in propertyEditors)
            {
                Panel.Children.Add(editor);
            }
        }
    }

    private static bool GetDefaultExpanded(MemberInfo propertyInfo)
    {
        var expandAttribute = propertyInfo.GetCustomAttribute<ExpandAttribute>();
        return expandAttribute?.DefaultExpanded ?? true;
    }
}
