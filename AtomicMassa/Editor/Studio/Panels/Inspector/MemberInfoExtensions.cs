namespace AtomicMassa.Editor.Studio.Panels.Inspector;

/// <summary>
/// Extra funcionality for the System.Reflection.MemberInfo class
/// </summary>
public static class MemberInfoExtensions
{
    /// <summary>
    /// Get the value of a member, either a property or a field
    /// </summary>
    /// <param name="member"></param>
    /// <param name="source"></param>
    /// <returns></returns>
    /// <exception cref="ArgumentException"></exception>
    public static object? GetValue(this MemberInfo member, object source)
    {
        return member switch
        {
            FieldInfo field => field.GetValue(source),
            PropertyInfo property when property.GetIndexParameters().Length == 0
                => property.GetValue(source),
            PropertyInfo property when property.GetIndexParameters().Length > 0 => null, // or handle indexed properties differently
            _
                => throw new ArgumentException(
                    "MemberInfo must be of type FieldInfo or PropertyInfo",
                    nameof(member)
                ),
        };
    }

    /// <summary>
    /// Set the value of a member, either a property or a field
    /// </summary>
    /// <param name="member"></param>
    /// <param name="target"></param>
    /// <param name="value"></param>
    /// <exception cref="ArgumentException"></exception>
    public static void SetValue(this MemberInfo member, object target, object value)
    {
        switch (member)
        {
            case FieldInfo field:
                field.SetValue(target, value);
                break;
            case PropertyInfo property:
                try
                {
                    if (property.CanWrite)
                    {
                        property.SetValue(target, value);
                    }
                }
                catch
                {
                    // throw new ArgumentException("PropertyInfo must have a setter", nameof(member));
                }
                break;
            default:
                throw new ArgumentException(
                    "MemberInfo must be of type FieldInfo or PropertyInfo",
                    nameof(member)
                );
        }
    }
}
