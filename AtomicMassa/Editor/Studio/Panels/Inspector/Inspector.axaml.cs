namespace AtomicMassa.Editor.Studio.Panels.Inspector;

/// <summary>
/// Inspector panel
/// </summary>
[Panel("Inspector")]
internal sealed partial class Inspector : UserControl
{
    public Inspector()
    {
        InitializeComponent();
        DataContext = App.Current.Services.GetRequiredService<InspectorViewModel>();
    }
}
