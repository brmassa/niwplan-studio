using Serilog.Events;

namespace AtomicMassa.Editor.Studio.Panels.Output;

/// <summary>
/// Represents a wrapper for a Serilog LogEvent with additional properties.
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="LogEventWrapper"/> class with a specified LogEvent.
/// </remarks>
/// <param name="logEvent">The Serilog LogEvent to be wrapped.</param>
public class LogEventWrapper(LogEvent logEvent)
{
    /// <summary>
    /// Gets or sets the actual LogEvent from Serilog.
    /// </summary>
    public LogEvent LogEvent { get; set; } = logEvent;

    /// <summary>
    /// Gets or sets the count of messages associated with the LogEvent. Default is 1.
    /// </summary>
    public int MessageCount { get; set; } = 1;
}
