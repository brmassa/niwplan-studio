using Avalonia.Data.Converters;

namespace AtomicMassa.Editor.Studio.Panels.Output;

/// <summary>
/// Converts the IsCollapsed property of an OutputViewModel to a boolean value.
/// </summary>
public class IsCollapsedConverter : IValueConverter
{
    /// <inheritdoc/>
    public object Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        if (value is OutputViewModel viewModel)
        {
            return viewModel.IsCollapsed;
        }
        return false;
    }

    /// <inheritdoc/>
    public object ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}
