namespace AtomicMassa.Editor.Studio.Panels.Output;

/// <summary>
/// Output panel
/// </summary>
[Panel("Output")]
internal sealed partial class Output : UserControl
{
    internal OutputViewModel OutputViewModel
    {
        get => (DataContext as OutputViewModel)!;
        set => DataContext = value;
    }

    public Output()
    {
        OutputViewModel = new();
        InitializeComponent();
    }

    public void Clear(object sender, RoutedEventArgs args)
    {
        OutputViewModel.Clear();
    }

    /// <summary>
    /// Copy Message To Clipboard
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    private void CopyMessageToClipboard(object sender, PointerPressedEventArgs args)
    {
        var point = args.GetCurrentPoint(sender as Control);

        if (sender is TextBlock control
            && !string.IsNullOrEmpty(control.Text))
        {
            if (point.Properties.IsRightButtonPressed)
            {
                OutputViewModel.Clipboard(this, control.Text);
            }
        }
    }
}
