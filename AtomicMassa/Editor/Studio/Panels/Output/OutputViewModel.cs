using Serilog.Events;
using System.Collections.Specialized;
using System.ComponentModel;

namespace AtomicMassa.Editor.Studio.Panels.Output;

/// <summary>
/// The view model of the Output panel
/// </summary>
internal sealed partial class OutputViewModel : ObservableObject
{
    public static ObservableCollection<LogEvent> LogFull => App.LogCollection;
    public ObservableCollection<LogEventWrapper> LogEvents { get; } = [];
    public ObservableCollection<LogEventWrapper> SelectedLogEvent { get; } = [];

    [ObservableProperty]
    private bool isInfoChecked = true;

    [ObservableProperty]
    private bool isWarningChecked = true;

    [ObservableProperty]
    private bool isErrorChecked = true;

    [ObservableProperty]
    private bool isCollapsed = true;

    [ObservableProperty]
    private string searchText = string.Empty;

    [ObservableProperty]
    private string messageDetails = string.Empty;

    public bool ClearOnPlay { get; set; } = true;

    public bool ClearOnBuild { get; set; } = true;

    public OutputViewModel()
    {
        FilterLogEvents();

        SelectedLogEvent.CollectionChanged += SelectedLogEventsChanged;

        PropertyChanged += FilterOnPropertyChanged;
    }

    public void Clear()
    {
        LogFull.Clear();
        FilterLogEvents();
    }

    /// <summary>
    /// Copy the log message to the clipboard
    /// </summary>
    public static void Clipboard(UserControl control, string message)
    {
        var clipboard = TopLevel.GetTopLevel(control)?.Clipboard;
        _ = (clipboard?.SetTextAsync(message));
    }

    public void SetMessageDetails(LogEventWrapper logEvent)
    {
        MessageDetails = logEvent.LogEvent.RenderMessage(CultureInfo.CurrentCulture);
    }

    private void SelectedLogEventsChanged(object? sender, NotifyCollectionChangedEventArgs e)
    {
        if (e.Action == NotifyCollectionChangedAction.Add)
        {
            if (e.NewItems != null && e.NewItems.Count > 0 && e.NewItems[0] is LogEventWrapper logEvent)
            {
                SetMessageDetails(logEvent);
            }
        }
    }

    private void FilterOnPropertyChanged(object? sender, PropertyChangedEventArgs e)
    {
        if (e.PropertyName != nameof(MessageDetails))
        {
            FilterLogEvents();
        }
    }

    private void FilterLogEvents()
    {
        SelectedLogEvent.Clear();
        LogEvents.Clear();

        var filteredLogs = LogFull
            .Where(log =>
                (IsInfoChecked || (log.Level != LogEventLevel.Information && log.Level != LogEventLevel.Debug)) &&
                (IsWarningChecked || log.Level != LogEventLevel.Warning) &&
                (IsErrorChecked || log.Level != LogEventLevel.Error) &&
                (string.IsNullOrEmpty(SearchText) || log.RenderMessage(CultureInfo.InvariantCulture).Contains(SearchText, StringComparison.OrdinalIgnoreCase)))
            .Select(log => new LogEventWrapper(log))
            .ToList();

        if (IsCollapsed)
        {
            foreach (var group in filteredLogs.GroupBy(l => l.LogEvent.RenderMessage(CultureInfo.InvariantCulture)))
            {
                LogEvents.Add(new LogEventWrapper(group.First().LogEvent) { MessageCount = group.Count() });
            }
        }
        else
        {
            foreach (var wrapper in filteredLogs)
            {
                LogEvents.Add(wrapper);
            }
        }
    }
}

