using AtomicMassa.Engine.Core.Asset;

namespace AtomicMassa.Editor.Studio.Panels.AssetBrowser;

/// <summary>
/// Represents a node in the asset hierarchy.
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="AssetBrowserNode"/> class with the specified directory information.
/// </remarks>
/// <param name="directoryInfo">The file system information for the directory.</param>
public sealed class AssetBrowserNode(FileSystemInfo directoryInfo)
{
    /// <summary>
    /// Gets or sets the name of the asset node.
    /// </summary>
    /// <value>
    /// The name of the asset node.
    /// </value>
    public string? Name { get; init; } = directoryInfo.Name.Replace(".meta.json", "", StringComparison.InvariantCulture);

    /// <summary>
    /// Load the asset of this given path.
    /// </summary>
    public Asset? Asset
    {
        get
        {
            try
            {
                return Asset.Load(AbsolutePath);
            }
            catch
            {
                return null;
            }
        }
    }

    private string AbsolutePath { get; init; } = directoryInfo.FullName;

    /// <summary>
    /// Gets or sets the subfolders contained within this asset node.
    /// </summary>
    /// <value>
    /// A collection of <see cref="AssetBrowserNode"/> objects representing the subfolders.
    /// </value>
    public ObservableCollection<AssetBrowserNode> Subfolders { get; } = [];
}
