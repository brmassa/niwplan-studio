using System.Collections.Specialized;

namespace AtomicMassa.Editor.Studio.Windows.MainWindow.AssetTab;

internal sealed partial class AssetTab : UserControl
{
    private const uint tabScrollJump = 100;

    private AssetTabViewModel AssetTabViewModel
    {
        get => (DataContext as AssetTabViewModel)!;
        set => DataContext = value;
    }

    public AssetTab()
    {
        AssetTabViewModel = App.Current.Services.GetRequiredService<AssetTabViewModel>();
        AssetTabViewModel.Assets.CollectionChanged += AssetsTabCollectionChanged;

        InitializeComponent();

        assetTab.SelectionChanged += AssetsSelectionChanged;

        AssetTabUpdateScrollButtonsVisibility();
    }

    private void AssetsTabCollectionChanged(object? sender, NotifyCollectionChangedEventArgs e)
    {
        if (e is { Action: NotifyCollectionChangedAction.Add, NewItems: not null }
            && e.NewItems[0] is AssetWrapper _)
        {
            Dispatcher.UIThread.Post(() =>
            {
                assetTab.SelectedIndex = AssetTabViewModel.Assets.Count - 1;
            });
        }
        AssetTabUpdateScrollButtonsVisibility();
    }

    private void AssetTabOnScrollClick(object sender, RoutedEventArgs _)
    {
        if (sender is Button button && int.TryParse(button.Tag?.ToString(), out var direction))
        {
            var scrollAmount = direction * tabScrollJump;

            tabStripScrollViewer.Offset = new Vector(tabStripScrollViewer.Offset.X + scrollAmount, tabStripScrollViewer.Offset.Y);  // Change 100 to the amount you want to scroll
            AssetTabUpdateScrollButtonsVisibility();
        }
    }

    private async void AssetTabUpdateScrollButtonsVisibility()
    {
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            leftScrollButton.IsVisible = tabStripScrollViewer.Offset.X > 0;
            rightScrollButton.IsVisible = tabStripScrollViewer.Offset.X < tabStripScrollViewer.Extent.Width - tabStripScrollViewer.Viewport.Width;
        });
    }

    private void AssetsSelectionChanged(object? sender, SelectionChangedEventArgs _)
    {
        AssetTabUpdateScrollButtonsVisibility();
    }

    private void AssetsChanged(object? sender, NotifyCollectionChangedEventArgs _)
    {
        AssetTabUpdateScrollButtonsVisibility();
    }

    private void OnTabClick(object sender, PointerPressedEventArgs e)
    {
        var properties = e.GetCurrentPoint(null).Properties;
        if (properties.IsLeftButtonPressed)
        {
            OnTabSelect(sender, null);
        }
        else if (properties.IsMiddleButtonPressed)
        {
            OnTabRemove(sender, null);
        }
    }

    private void OnTabSelect(object? sender, RoutedEventArgs? _)
    {
        if (sender is Control { DataContext: AssetWrapper asset })
        {
            AssetTabViewModel.SelectAsset(asset);
        }
    }

    private void OnTabRemove(object? sender, RoutedEventArgs? _)
    {
        if (sender is Control { DataContext: AssetWrapper asset })
        {
            AssetTabViewModel.RemoveAsset(asset);
            assetTab.SelectedIndex = AssetTabViewModel.Assets.Count - 1;
        }
    }
}
