using AtomicMassa.Editor.Build.Settings;
using AtomicMassa.Editor.Studio.Panels.AssetBrowser;
using AtomicMassa.Editor.Studio.Panels.Inspector;
using AtomicMassa.Editor.Studio.Panels.Output;
using AtomicMassa.Editor.Studio.Panels.SceneTree;
using AtomicMassa.Editor.Studio.Services.Settings;
using Avalonia.Platform.Storage;
using NP.Ava.UniDockService;
using System.Collections;
using System.Threading.Tasks;

namespace AtomicMassa.Editor.Studio.Windows.MainWindow;

/// <summary>
/// Main Window. Manages top main menus and panels.
/// </summary>
internal sealed partial class MainWindow : Window
{
    private const string appName = "Atomic Massa";
	private IUniDockService uniDockService = null!;
    private BuildManager buildManager = null!;

    public MainWindow()
    {
        Initialize();
    }

    private async void Initialize()
    {
        var appSettingsLocal = await CheckProjectArgument().ConfigureAwait(true);
        var appSettings = App.Current.Services.GetRequiredService<IAppSettings>();
        _ = appSettings.Load(appSettingsLocal);
        DataContext = App.Current.Services.GetRequiredService<MainWindowViewModel>();
        (DataContext as MainWindowViewModel)!.OnCompilationCompleted += OnCompilationCompleted;
        Title = $"{appSettings.Title} - {appName}";

        buildManager = App.Current.Services.GetRequiredService<BuildManager>();

        InitializeComponent();

        await MenuReloadItems(this).ConfigureAwait(true);

        // set the uniDockService interface to contain the reference to the
        // dock manager defined as a resource.
        uniDockService = (IUniDockService)this.FindResource("TheDockManager")!;
        uniDockService.DockItemsViewModels = [];

        // FIXME: create a default dock layout if none exists
        AddDefaultDocks();
    }

    private void AddDefaultDocks()
    {
        AddDock("LeftPanel", "Scene Tree", new SceneTree());
        AddDock("Inspector", "Inspector", new Inspector());
        AddDock("BottomPanel", "Output", new Output());
        AddDock("BottomPanel", "Assets", new AssetBrowser());
    }

    public void AddDock(string dockGroupId, string title, UserControl content)
    {
        uniDockService.DockItemsViewModels.Add
        (
            new DockItemViewModelBase
            {
                DockId = Guid.NewGuid().ToString(),
                Header = title,
                Content = content,
                DefaultDockGroupId = dockGroupId,
                // DefaultDockOrderInGroup = _tabNumber,
                IsSelected = true,
                IsActive = true
            }
            );
    }

    private static readonly FilePickerOpenOptions filePickerOpenOptions = new()
    {
        Title = "Select .app.json File",
        AllowMultiple = false,
        FileTypeFilter = [
                new("App Settings")
                { Patterns = ["*.app.json"] }
            ]
    };

    public static async Task MenuReloadItems(Window window)
    {
        if (window is not MainWindow mainWindow)
        {
            return;
        }
        await mainWindow.MenuReset().ConfigureAwait(true);
        await mainWindow.MenuLoadItems().ConfigureAwait(true);
        await mainWindow.MenuLoadPanelItems().ConfigureAwait(true);
    }

    public async Task MenuReset()
    {
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var items = mainMenu.Items as IList;
            items?.Clear();
        });
    }

    private class MethodInfoWithAttributes
    {
        /// <summary>
        /// Method info
        /// </summary>
        public required MethodInfo Method { get; set; }

        /// <summary>
        /// List if attributes of the method
        /// </summary>
        public required List<MenuItemAttribute> Attributes { get; set; }
    }

    /// <summary>
    /// Loads menu items based on methods decorated with the <see cref="MenuItemAttribute"/>.
    /// </summary>
    private async Task MenuLoadItems()
    {
        var allMethods = buildManager.LoadedAssemblies
                .SelectMany(assembly => assembly.GetLoadableTypes())
                .SelectMany(type => type.GetMethods(BindingFlags.Public | BindingFlags.Static))
                .Select(method => new MethodInfoWithAttributes
                {
                    Method = method,
                    Attributes = method.GetCustomAttributes<MenuItemAttribute>(false).ToList()
                })
                .Where(mwa => mwa.Attributes.Count > 0 && mwa.Method.GetParameters().Length <= 1);

        foreach (var method in allMethods)
        {
            foreach (var attribute in method.Attributes)
            {
                void MenuClickCallback(object? sender, RoutedEventArgs _)
                {
                    Task.Run(() =>
                    {
                        try
                        {
                            method.Method.Invoke(null, [this]);
                        }
                        catch (Exception ex)
                        {
                            Log.Error("Failed to invoke method: {Message}", ex.Message);
                        }
                    });
                }

                // Ensure UI operations are dispatched to the UI thread.
                await Dispatcher.UIThread.InvokeAsync(() =>
                {
                    CreateMenuPath(attribute.Path, MenuClickCallback);
                });
            }
        }
    }

    /// <summary>
    /// Loads panel menu items based on methods decorated with the <see cref="PanelAttribute"/>.
    /// </summary>
    private async Task MenuLoadPanelItems()
    {
        var panelTypes = buildManager.LoadedAssemblies
            .SelectMany(assembly => assembly.GetLoadableTypes())
            .Where(type => type.GetCustomAttributes<PanelAttribute>(false).Any());

        foreach (var panelType in panelTypes)
        {
            if (panelType.GetCustomAttribute(typeof(PanelAttribute)) is PanelAttribute attribute)
            {
                void ClickHandler(object? sender, RoutedEventArgs _)
                {
                    if (Activator.CreateInstance(panelType) is UserControl panelInstance)
                    {
                        // Ensure UI operations are dispatched to the UI thread.
                        Dispatcher.UIThread.Post(() =>
                        {
                            AddDock("LeftPanel", attribute.Name, panelInstance);
                        });
                    }
                }

                // Ensure UI operations are dispatched to the UI thread.
                await Dispatcher.UIThread.InvokeAsync(() =>
                {
                    CreateMenuPath(attribute.Path, ClickHandler);
                });
            }
        }
    }

    /// <summary>
    /// Create a menu path from a string.
    /// </summary>
    /// <param name="path">The path to create, in the form "Menu/Item/SubItem"</param>
    /// <param name="clickHandler">An optional click handler to add to the last item in the path.</param>
    /// <remarks>
    /// If the path already exists, the click handler will be added to the last item in the path.
    /// </remarks>
    private void CreateMenuPath(string path, EventHandler<RoutedEventArgs> clickHandler)
    {
        var menuItems = path.Split('/');
        var currentItems = (mainMenu.Items as IList)!;
        MenuItem? lastMenuItem = null;

        foreach (var menuItem in menuItems)
        {
            var existingMenuItem = currentItems
                ?.OfType<MenuItem>()
                .FirstOrDefault(item => item.Header?.ToString() == menuItem);

            if (existingMenuItem == null)
            {
                MenuItem newMenuItem = new() { Header = menuItem };
                _ = (currentItems?.Add(newMenuItem));
                existingMenuItem = newMenuItem;
            }

            lastMenuItem = existingMenuItem;
            currentItems = existingMenuItem.Items;
        }

        // Add the click handler to the last menu item in the path, if it was provided.
        if (lastMenuItem != null)
        {
            lastMenuItem.Click += clickHandler;
        }
    }

    private async Task<AppSettings> CheckProjectArgument()
    {
        var args = Environment.GetCommandLineArgs();
        var appSettingsPath = string.Empty;

        if (args.Contains("--project"))
        {
            var index = Array.IndexOf(args, "--project");
            if (index < args.Length - 1)
            {
                appSettingsPath = args[index + 1];
            }
        }

        AppSettings? appSettings = null;

        while (appSettings == null)
        {
            if (
                appSettingsPath == null
                || !File.Exists(appSettingsPath)
                || !appSettingsPath.EndsWith(".app.json", StringComparison.InvariantCultureIgnoreCase)
            )
            {
                var topLevel = GetTopLevel(this)!;
                IReadOnlyList<IStorageFile>? result = null;

                // Ensure UI operations are dispatched to the UI thread.
                await Dispatcher.UIThread.InvokeAsync(async () =>
                {
                    result = await topLevel.StorageProvider.OpenFilePickerAsync(filePickerOpenOptions);
                }).ConfigureAwait(true);

                if (result != null && result.Count > 0)
                {
                    appSettingsPath = result[0].Path.LocalPath;
                }
                else
                {
                    if (Application.Current!.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktopLifetime)
                    {
                        desktopLifetime.Shutdown();
                        appSettingsPath = null;
                    }
                }
            }

            if (appSettingsPath is not null)
            {
                appSettingsPath = Path.GetFullPath(appSettingsPath);

                try
                {
                    appSettings = AmObject.Load<AppSettings>(appSettingsPath);
                    appSettings!.AssetJsonAbsolutePath = appSettingsPath;
                }
                catch
                {
                    appSettingsPath = null;
                }
            }
        }
        return appSettings;
    }

    private async void OnCompilationCompleted()
    {
        await MenuReloadItems(this);
    }
}
