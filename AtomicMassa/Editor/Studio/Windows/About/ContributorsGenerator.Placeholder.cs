// namespace AtomicMassa.Editor.Studio;

// #if !ContributorsSourceGenerated
// /// <summary>
// /// A static class with the list of contributors and the compilation date.
// /// </summary>
// public static partial class Contributors
// {
//     /// <summary>
//     /// The compilation date. Format: yyyy-MM-dd
//     /// </summary>
//     public const string List = "";

//     /// <summary>
//     /// The compilation date. Format: yyyy-MM-dd
//     /// </summary>
//     public const string CompilationDate = "";
// }
// #else
// [GenerateMyGeneratedClass]
// public static partial class Contributors { }
// #endif
