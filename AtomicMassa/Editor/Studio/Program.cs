namespace AtomicMassa.Editor.Studio;

internal static class Program
{
    [STAThread]
    public static void Main(string[] args)
    {
        BuildManager.MsBuildLocatorRegisterDefaults();
        _ = BuildAvaloniaApp().StartWithClassicDesktopLifetime(args);
    }

    public static AppBuilder BuildAvaloniaApp()
    {
        return AppBuilder.Configure<App>().UsePlatformDetect().LogToTrace();
    }
}
