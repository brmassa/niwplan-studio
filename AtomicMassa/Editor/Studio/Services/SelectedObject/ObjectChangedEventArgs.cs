namespace AtomicMassa.Editor.Studio.Services.SelectedObject;

/// <summary>
/// <see cref="ISelectedObjectProvider{T}"/> returns this argument when the selected object changes.
/// </summary>
/// <typeparam name="T">
/// The type of object that has been selected.
/// </typeparam>
public class ObjectChangedEventArgs<T> : EventArgs
{
    /// <summary>
    /// Gets or sets the new object that was selected.
    /// </summary>
    /// <value>
    /// The new selected object.
    /// </value>
    public T? New { get; set; }

    /// <summary>
    /// Gets or sets the old object that was selected.
    /// </summary>
    /// <value>
    /// The old selected object.
    /// </value>
    public T? Old { get; set; }
}
