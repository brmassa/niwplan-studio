# Editor

Some of the code is not shipped in the final game. It's only used during the creation.

* [Studio](./Studio/): the interface of the game creator itself. Some engines call it simply *Editor*.
* [CSharp](./CSharp/): C# source generators and analyzers. They help to create code automatically.
