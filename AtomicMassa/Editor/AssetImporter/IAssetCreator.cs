using AtomicMassa.Engine.Core.Asset;

namespace AtomicMassa.Editor.AssetImporter;

/// <summary>
/// Asset Importer class
/// </summary>
public interface IAssetImporter
{
    /// <summary>
    /// Check if the importer accept to import the asset, based on the file path
    /// </summary>
    /// <param name="filePath"></param>
    /// <returns></returns>
    bool IsValid(string filePath);

    /// <summary>
    /// Instantiate the asset
    /// </summary>
    /// <param name="filePath"></param>
    /// <returns></returns>
    Asset CreateAsset(string filePath);
}
