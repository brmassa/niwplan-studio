using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.Build.Locator;
using Serilog;

namespace AtomicMassa.Editor.Build;

/// <summary>
/// User code compiler common tasks
/// </summary>
/// <remarks>
/// .ctor
/// </remarks>
/// <param name="settings"></param>
/// <param name="logger"></param>
/// <exception cref="InvalidOperationException"></exception>
public abstract class CompilerBase(IAppSettings settings, ILogger logger)
{
    /// <summary>
    /// App settings
    /// </summary>
    protected readonly BuildAppSettings Settings = new BuildAppSettings().Load(settings) as BuildAppSettings
                   ?? throw new InvalidOperationException("Settings could not be loaded.");

    /// <summary>
    /// Serilog
    /// </summary>
    protected readonly ILogger Logger = logger;

    /// <summary>
    /// Initial setup
    /// </summary>
    /// <param name="generateCsProjFile"></param>
    /// <returns></returns>
    /// <exception cref="InvalidOperationException"></exception>
    protected async Task<string> SetupAsync(Func<string> generateCsProjFile)
    {
        ArgumentNullException.ThrowIfNull(generateCsProjFile);

        // Ensure MSBuildLocator is registered
        if (!MSBuildLocator.IsRegistered)
        {
            MSBuildLocator.RegisterDefaults();
        }

        // Check if the .csproj file exists, and create it if it doesn't
        var csprojFilePath = generateCsProjFile();
        if (string.IsNullOrEmpty(csprojFilePath))
        {
            Logger.Error("Compilation failed with errors:");
            throw new InvalidOperationException("Compilation failed.");
        }

        // Restore packages (nuget packages)
        try
        {
            await RestorePackages(csprojFilePath);
        }
        catch (Exception e)
        {
            Logger.Error("Restoring failed: {error}", e);
        }

        return csprojFilePath;
    }

    /// <summary>
    /// Certify the creaation of all directories.
    /// </summary>
    /// <param name="assemblyOutputPath"></param>
    protected static void CreateOutputPathDirectories(string assemblyOutputPath)
    {
        var directoryPath = Path.GetDirectoryName(assemblyOutputPath);
        if (!string.IsNullOrEmpty(directoryPath) && !Directory.Exists(directoryPath))
        {
            Directory.CreateDirectory(directoryPath);
        }
    }

    /// <summary>
    /// Restore Nuget packages
    /// </summary>
    /// <param name="csprojFilePath"></param>
    /// <returns></returns>
    /// <exception cref="InvalidOperationException"></exception>
    public async Task RestorePackages(string csprojFilePath)
    {
        var folder = Path.GetDirectoryName(csprojFilePath);
        Logger.Information("Package restoring: {Path}", folder);
        var startInfo = new ProcessStartInfo("dotnet", $"restore \"{folder}\"")
        {
            CreateNoWindow = true,
            UseShellExecute = false,
            RedirectStandardOutput = true,
            RedirectStandardError = true
        };

        using var process = new Process { StartInfo = startInfo };
        process.Start();
        var output = await process.StandardOutput.ReadToEndAsync();
        // var errors = await process.StandardError.ReadToEndAsync();
        process.WaitForExit();

        if (process.ExitCode != 0)
        {
            Logger.Error("Package restore failed with errors: {Errors}", output);
            throw new InvalidOperationException("Package restore failed.");
        }

        Logger.Information("Package restore succeeded");
    }

    /// <summary>
    /// Create the output directory for the compile process
    /// </summary>
    /// <param name="csprojFilePath"></param>
    /// <param name="now"></param>
    /// <returns></returns>
    protected string CompileOutputPath(string csprojFilePath, string now)
    {
        return Path.Combine(
          Path.GetDirectoryName(csprojFilePath) ?? string.Empty,
          "bin",
          "Debug",
          Settings.TargetFramework,
          $"{Settings.TitleToPathFriendly}-{now}.dll");
    }

    /// <summary>
    /// Create the output directory for the export process
    /// </summary>
    /// <param name="absolutePath"></param>
    /// <param name="pathInter"></param>
    /// <returns></returns>
    protected static string ExportOutputDir(string absolutePath, string pathInter)
    {
        return Path.Combine(absolutePath, pathInter);
    }
}
