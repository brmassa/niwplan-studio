using System.CommandLine;
using System.Text.Json;
using System.Threading.Tasks;
using Serilog;

namespace AtomicMassa.Editor.Build;

/// <summary>
/// Terminal application
/// </summary>
public static class Program
{
    /// <summary>
    /// Entry point
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public static async Task<int> Main(string[] args)
    {
        // Set up Serilog
        Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Debug()
            .Enrich.FromLogContext()
            .WriteTo.Console(formatProvider: System.Globalization.CultureInfo.CurrentCulture)
            .CreateLogger();

        var projectPathArgument = ProjectPathArgument();

        var compileCommand = CompileCommand(projectPathArgument);

        var buildCommand = ExportCommand(projectPathArgument);

        RootCommand rootCommand = new("Atomic Massa terminal operations")
        {
            compileCommand, buildCommand
        };

        try
        {
            await rootCommand.InvokeAsync(args);
            return 0;
        }
        catch (FileNotFoundException ex)
        {
            Log.Logger.Error(ex.Message);
            return 1;
        }
        catch (JsonException ex)
        {
            Log.Logger.Error(ex.Message);
            return 1;
        }
        catch (InvalidOperationException ex)
        {
            Log.Logger.Error(ex.Message);
            throw;
        }
        catch (Exception ex)
        {
            Log.Logger.Error(ex.Message);
            throw;
        }
    }

    private static Argument<FileSystemInfo> ProjectPathArgument()
    {
        var projectPathArgument = new Argument<FileSystemInfo>
            (name: "ProjectPath",
            description: "Project path (either the folder or the '.app.json' file)",
            getDefaultValue: () => new DirectoryInfo("./"));
        projectPathArgument.AddValidator(result =>
        {
            var projectPathInfo = result.GetValueForArgument(projectPathArgument);

            var projectPath = GetFileInfo(projectPathInfo);

            try
            {
                if (!projectPath.Exists)
                {
                    throw new FileNotFoundException("Not a valid project path");
                }
                ReadAppSettings(projectPath);
            }
            catch (FileNotFoundException ex)
            {
                result.ErrorMessage = ex.Message;
                throw;
            }
            catch (JsonException ex)
            {
                result.ErrorMessage = ex.Message;
                throw;
            }
        });
        return projectPathArgument;
    }

    private static Command ExportCommand(Argument<FileSystemInfo> projectPathArgument)
    {
        var buildCommand = new Command("export", "Export the project to a given platform")
        {
            projectPathArgument
        };
        buildCommand.SetHandler(async (projectPathInfo) =>
        {
            Log.Logger.Information("exporting...");
            var buildAppSettings = CreateCompiler(projectPathInfo);
            IUserCodeCompiler builder = new CompileUserCode(buildAppSettings, Log.Logger);
            await builder.ExecuteAsync();
            builder = new ExportUserCode(buildAppSettings, Log.Logger);
            await builder.ExecuteAsync();
        },
            projectPathArgument);
        return buildCommand;
    }

    private static Command CompileCommand(Argument<FileSystemInfo> projectPathArgument)
    {
        var compileCommand = new Command("compile", "Compile the project")
        {
            projectPathArgument
        };
        compileCommand.SetHandler(async (projectPathInfo) =>
        {
            Log.Logger.Information("compiling...");
            var buildAppSettings = CreateCompiler(projectPathInfo);
            var builder = new CompileUserCode(buildAppSettings, Log.Logger);
            await builder.ExecuteAsync();
        },
            projectPathArgument);
        return compileCommand;
    }

    private static BuildAppSettings CreateCompiler(FileSystemInfo projectPathInfo)
    {
        var projectPath = GetFileInfo(projectPathInfo);
        var buildAppSettings = ReadAppSettings(projectPath);
        Log.Logger.Information($"{buildAppSettings.Title}");
        return buildAppSettings;
    }

    private static FileInfo GetFileInfo(FileSystemInfo projectPathInfo)
    {
        return projectPathInfo switch
        {
            FileInfo file => file,
            DirectoryInfo directory => directory.GetFiles("*.app.json").FirstOrDefault() ??
                                throw new FileNotFoundException("Not a valid project path"),
            _ => throw new FileNotFoundException("Not a valid project path"),
        };
    }

    private static BuildAppSettings ReadAppSettings(FileInfo projectfullPath)
    {
        ArgumentNullException.ThrowIfNull(projectfullPath);

        try
        {
            var appSettings = AmObject.Load<BuildAppSettings>(projectfullPath.FullName);

            // FIXME: test if it's a valid appSettings json file
            if (string.IsNullOrEmpty(appSettings?.Title))
            {
                throw new JsonException("File is not a valid AppSettings");
            }
            appSettings.AssetJsonAbsolutePath = projectfullPath.FullName;
            return appSettings;
        }
        catch
        {
            throw new JsonException("File is not a valid AppSettings");
        }
    }
}
