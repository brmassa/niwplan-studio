using System.Threading.Tasks;

namespace AtomicMassa.Editor.Build;

/// <summary>
/// User code compiler interface
/// </summary>
public interface IUserCodeCompiler
{
    /// <summary>
    /// Execute the operation
    /// </summary>
    /// <returns></returns>
    Task<string> ExecuteAsync();
}
