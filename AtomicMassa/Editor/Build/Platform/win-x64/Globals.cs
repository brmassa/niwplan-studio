﻿global using Microsoft.Extensions.DependencyInjection;
global using Microsoft.Extensions.Hosting;

global using Serilog;
global using Silk.NET.Input;
global using Silk.NET.Maths;
global using Silk.NET.Vulkan;
global using Silk.NET.Windowing;

global using System.Globalization;
global using System.Runtime.InteropServices;
global using System.Text.Json;

global using AtomicMassa.Engine;
global using AtomicMassa.Engine.Core.Asset.Database;
global using AtomicMassa.Engine.Core.Camera;
global using AtomicMassa.Engine.Core.Graphics;
global using AtomicMassa.Engine.Core.Nodes;
global using AtomicMassa.Engine.Core.Utils;
global using AtomicMassa.Engine.Core.Utils.File;
global using AtomicMassa.Engine.ImGui;
global using AtomicMassa.Types;

namespace AtomicMassa;
