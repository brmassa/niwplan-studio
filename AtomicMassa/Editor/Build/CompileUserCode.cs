using System.Globalization;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.MSBuild;
using Serilog;

namespace AtomicMassa.Editor.Build;

/// <summary>
/// Compile the user code as a library
/// </summary>
/// <remarks>
/// ctor
/// </remarks>
/// <param name="settings"></param>
/// <param name="logger"></param>
public class CompileUserCode(IAppSettings settings, ILogger logger) : CompilerBase(settings, logger), IUserCodeCompiler
{
    /// <summary>
    /// Execture the action
    /// </summary>
    /// <returns></returns>
    /// <exception cref="InvalidOperationException"></exception>
    public async Task<string> ExecuteAsync()
    {
        var csprojFilePath = await SetupAsync(() => GenerateUserCodeCsProjFile());
        CreateOutputPathDirectories(csprojFilePath);

        // Load the project
        using var workspace = MSBuildWorkspace.Create();
        var project = await workspace.OpenProjectAsync(csprojFilePath);
        var compilation = await project.GetCompilationAsync() ?? throw new InvalidOperationException("Compilation failed.");

        // Compile the project
        var now = DateTime.UtcNow.ToString("yyyyMMdd-HHmmss", CultureInfo.InvariantCulture);
        var assemblyOutputPath = CompileOutputPath(csprojFilePath, now);
        CreateOutputPathDirectories(assemblyOutputPath);
        using (var fs = new FileStream(assemblyOutputPath, FileMode.Create))
        {
            var result = compilation.Emit(fs);
            if (!result.Success)
            {
                // Handle compilation failures
                Logger.Error("Compilation failed with errors:");
                foreach (var diagnostic in result.Diagnostics)
                {
                    Logger.Error("{errorId}: {errorMessage}\n{location}",
                        diagnostic.Id,
                        diagnostic.GetMessage(CultureInfo.InvariantCulture),
                        diagnostic.Location);
                }
                throw new InvalidOperationException("Compilation failed");
            }
        }

        workspace.CloseSolution();

        // Compilation succeeded
        Logger.Information("Compilation succeeded.");
        return assemblyOutputPath;
    }

    private string GenerateUserCodeCsProjFile()
    {
        var project = CsProjectGenerator.GenerateUserCode(Settings, Logger);
        project.Save();
        return project.FullPath;
    }

}
