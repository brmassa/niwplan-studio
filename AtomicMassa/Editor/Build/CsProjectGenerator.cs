using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Build.Construction;
using Serilog;

namespace AtomicMassa.Editor.Build;

/// <summary>
/// Create CSProj
/// </summary>
public static class CsProjectGenerator
{
    /// <summary>
    /// Generate a brand new .csproj file
    /// </summary>
    /// <param name="settings"></param>
    /// <param name="logger"></param>
    /// <returns></returns>
    /// <exception cref="InvalidOperationException"></exception>
    public static ProjectRootElement GenerateUserCode(IBuildAppSettings settings, ILogger logger)
    {
        ArgumentNullException.ThrowIfNull(settings);
        ArgumentNullException.ThrowIfNull(logger);
        var csprojFilePath = UserCodeCsProjFullPath(settings);
        var isExectutable = false;

        var projectRoot = ProjectRootElement.Create(csprojFilePath);
        projectRoot.Sdk = settings.TargetSdk;
        projectRoot
            .AddProjectInfo(settings, executable: isExectutable)
            .AddAtomicMassaDir()
            .AddInternalDllReferences(settings, isExectutable)
            .AddNugetPackageReferences(settings)
            .AddCsFiles(settings);

        return projectRoot;
    }

    private static string UserCodeCsProjFullPath(IBuildAppSettings settings) => Path.Combine(
        settings.CacheAbsoluteDir,
        "Source",
        $"{settings.TitleToPathFriendly}.csproj");

    /// <summary>
    /// Generate a brand new .csproj file
    /// </summary>
    /// <param name="settings"></param>
    /// <param name="logger"></param>
    /// <returns></returns>
    /// <exception cref="InvalidOperationException"></exception>
    public static ProjectRootElement GenerateExecutable(IBuildAppSettings settings, ILogger logger)
    {
        ArgumentNullException.ThrowIfNull(settings);
        ArgumentNullException.ThrowIfNull(logger);
        var csprojFilePath = Path.Combine(settings.CacheAbsoluteDir, settings.ExportSourceSubDir, "project.csproj");
        var isExectutable = true;

        var projectRoot = ProjectRootElement.Create(csprojFilePath);
        projectRoot.Sdk = settings.TargetSdk;
        projectRoot
            .AddProjectInfo(settings, isExectutable)
            .AddAtomicMassaDir()
            .AddInternalDllReferences(settings, isExectutable)
            .AddUserProject(settings)
            .AddNugetPackageReferences(settings)
            .AddAsetsDir(settings)
            .CopyPlataformSpecificFiles(settings)
            ;

        return projectRoot;
    }

    /// <summary>
    /// Return the relative path from the build from the Solution
    /// </summary>
    /// <returns></returns>
    public static string AtomicMassaDir()
    {
        var assemblyLocation = Assembly.GetExecutingAssembly().Location;
        var directoryName = Path.GetDirectoryName(assemblyLocation);

        while (directoryName != null)
        {
            if (Directory.GetFiles(directoryName, "*.sln", SearchOption.TopDirectoryOnly).Length > 0)
            {
                return directoryName;
            }

            directoryName = Directory.GetParent(directoryName)?.FullName;
        }

        return string.Empty;
    }

    private static ProjectRootElement AddAtomicMassaDir(this ProjectRootElement projectRoot)
    {
        var atomicMassaPropGroup = projectRoot.AddPropertyGroup();
        atomicMassaPropGroup.AddProperty("AtomicMassaDir", AtomicMassaDir());
        return projectRoot;
    }

    private static ProjectRootElement AddAsetsDir(this ProjectRootElement projectRoot, IBuildAppSettings settings)
    {
        var assetsSourceFiles = projectRoot.AddItemGroup();
        var relativePath = Path.GetRelativePath(Path.Combine(settings.CacheAbsoluteDir, settings.ExportSourceSubDir), settings.AssetsAbsoluteDir);
        assetsSourceFiles.AddItem("AssetsSourceFiles", Path.Combine(relativePath, "**", "*"));

        var target = projectRoot.AddTarget("CopyAssetContent");
        target.AfterTargets = "PostBuildEvent";

        var copyTask = target.AddTask("Copy");
        copyTask.SetParameter("SourceFiles", "@(AssetsSourceFiles)");
        copyTask.SetParameter("DestinationFolder", "$(OutDir)/Assets");
        copyTask.Condition = "'$(Configuration)'=='Debug'";

        copyTask = target.AddTask("Copy");
        copyTask.SetParameter("SourceFiles", "@(AssetsSourceFiles)");
        copyTask.SetParameter("DestinationFolder", "$(PublishDir)/Assets");
        copyTask.Condition = "'$(Configuration)'=='Release'";

        return projectRoot;
    }

    private static ProjectRootElement AddCsFiles(this ProjectRootElement projectRoot, IBuildAppSettings settings)
    {
        var itemGroup = projectRoot.AddItemGroup();
        var item = itemGroup.AddItem("Compile", $"{settings.CacheSourceRelativeDir}/**/*.cs");
        item.Exclude = $"**/obj/**";
        return projectRoot;
    }

    private static ProjectRootElement AddProjectInfo(this ProjectRootElement projectRoot, IBuildAppSettings settings, bool executable)
    {
        var group = projectRoot.AddPropertyGroup();
        group.AddProperty("OutputType", executable ? "WinExe" : "Library");
        group.AddProperty("TargetFramework", settings.TargetFramework);
        group.AddProperty("ImplicitUsings", "enable");
        group.AddProperty("Nullable", "enable");

        if (executable)
        {
            group.AddProperty("DebugType", "None");
            group.AddProperty("DebugSymbols", "false");
            group.AddProperty("AllowUnsafeBlocks", "true");
            group.AddProperty("PublishSingleFile", "true");
            group.AddProperty("SelfContained", "true");
        }
        else
        {
            group.AddProperty("GenerateDocumentationFile", "true");
        }

        return projectRoot;
    }

    private static ProjectRootElement AddUserProject(this ProjectRootElement projectRoot, IBuildAppSettings settings)
    {
        var itemGroup = projectRoot.AddItemGroup();
        itemGroup.AddItem("ProjectReference",
        Path.GetRelativePath(Path.GetDirectoryName(projectRoot.FullPath)!, UserCodeCsProjFullPath(settings)));
        return projectRoot;
    }

    private static ProjectRootElement AddInternalDllReferences(this ProjectRootElement projectRoot, IBuildAppSettings settings, bool executable)
    {
        var itemGroup = projectRoot.AddItemGroup();
        foreach (var atomicMassaPackage in settings.AtomicMassaPackages)
        {
            itemGroup.AddItem("Reference", atomicMassaPackage.Item2)
                .AddMetadata("HintPath",
                    Path.Combine(
                        "$(AtomicMassaDir)",
                        atomicMassaPackage.Item1,
                        "bin",
                        "Debug",
                        settings.TargetFramework,
                        $"{atomicMassaPackage.Item2}.dll"
                    ),
                    true);
        }

        return projectRoot;
    }

    private static ProjectRootElement AddNugetPackageReferences(this ProjectRootElement projectRoot, IBuildAppSettings settings)
    {
        var itemGroup = projectRoot.AddItemGroup();
        foreach (var package in settings.PackageReferences)
        {
            itemGroup.AddItem("PackageReference", package.Item1)
                .AddMetadata("Version", package.Item2, true);
        }
        return projectRoot;
    }

    [Obsolete]
    private static ProjectRootElement AddInternalReferences(this ProjectRootElement projectRoot, IBuildAppSettings settings)
    {
        var itemGroup = projectRoot.AddItemGroup();
        foreach (var internalPackage in settings.InternalPackages)
        {
            itemGroup.AddItem("ProjectReference", $"$(AtomicMassaDir)/{internalPackage}.csproj");
        }
        return projectRoot;
    }

    [Obsolete]
    private static async Task<ProjectRootElement> AddNugetPackageReferencesOnline(this ProjectRootElement projectRoot, IBuildAppSettings settings)
    {
        var itemGroup = projectRoot.AddItemGroup();
        foreach (var package in settings.Packages)
        {
            itemGroup.AddItem("PackageReference", package)
                .AddMetadata("Version", await GetLatestNugetPackageVersion(package), true);
        }
        return projectRoot;
    }

    private static async Task<string> GetLatestNugetPackageVersion(string packageName)
    {
        using var httpClient = new HttpClient();
        var jsonString = await httpClient.GetStringAsync($"https://api.nuget.org/v3-flatcontainer/{packageName.ToUpperInvariant()}/index.json");
        var jsonDocument = System.Text.Json.JsonDocument.Parse(jsonString);
        var versionArray = jsonDocument.RootElement.GetProperty("versions");
        var latestVersion = versionArray.EnumerateArray()
            .Where(element => !(element.GetString() ?? string.Empty).Contains('-', StringComparison.InvariantCulture))
            .LastOrDefault().GetString()
            ?? string.Empty;
        return latestVersion;
    }

    private static void CopyPlataformSpecificFiles(this ProjectRootElement projectRoot, IBuildAppSettings settings, string platform = "win-x64")
    {
        var assemnlyCurrentDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        var fullPath = Path.GetFullPath($"{assemnlyCurrentDirectory}/Platform/{platform}/");

        var files = Directory.GetFiles(fullPath);
        var destDir = Path.Combine(Path.GetDirectoryName(projectRoot.FullPath)!, settings.SourceSubDir);

        // Copy each file to the target directory
        foreach (var file in files)
        {
            var fileName = Path.GetFileName(file);
            var destFile = Path.Combine(destDir, fileName);
            Directory.CreateDirectory(destDir);
            File.Copy(file, destFile, true);
        }
    }
}
