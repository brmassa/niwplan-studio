namespace AtomicMassa.Editor.Build.Settings;

/// <inheritdoc cref="IAppSettings"/>
public interface IBuildAppSettings : IAppSettings
{
    /// <summary>
    /// Get the absolute path of the Cache folder
    /// </summary>
    string CacheAbsoluteDir { get; }

    /// <summary>
    /// Get the relative path of the Soruce inside Cache folder
    /// </summary>
    string CacheSourceRelativeDir { get; }

    /// <summary>
    /// Get the folder that source code files are present
    /// </summary>
    string SourceSubDir { get; }

    /// <summary>
    /// Get the folder that export source files are present
    /// </summary>
    string ExportSourceSubDir { get; }

    /// <summary>
    /// List of external packages
    /// </summary>
    string[] Packages { get; }

    /// <summary>
    /// List of external packages
    /// </summary>
    (string, string)[] PackageReferences { get; }

    /// <summary>
    /// List of internal engine packages
    /// </summary>
    string[] InternalPackages { get; }

    /// <summary>
    /// List of internal engine packages
    /// </summary>
    (string, string)[] AtomicMassaPackages { get; }

    /// <summary>
    /// The target Framework
    /// </summary>
    string TargetFramework { get; }

    /// <summary>
    /// The target SDK
    /// </summary>
    string TargetSdk { get; }
}
