namespace AtomicMassa.Engine.Core.Asset;

/// <summary>
/// Represents an asset with a file path.
/// </summary>
public class Asset : AmObject
{
    /// <summary>
    /// Gets or sets the file path of the asset.
    /// </summary>
    [HideInEditor]
    public string RelativePath { get; set; } = string.Empty;

    /// <summary>
    /// Loads the content from the specified absolute path.
    /// </summary>
    /// <param name="absolutePath">The absolute path to load content from.</param>
    /// <returns>An instance of <see cref="AmObject"/> if the content is successfully loaded; otherwise, an exception is thrown.</returns>
    /// <exception cref="Exception">Thrown when the content fails to load from the given path.</exception>
    public static Asset? Load(string absolutePath)
    {
        if (File.Exists(absolutePath))
        {
            return Load<Asset>(absolutePath);
        }
        throw new FileNotFoundException("Node load failed");
    }
}