using AtomicMassa.Engine.Core.Asset;

namespace AtomicMassa.Engine.Core.Nodes;

/// <summary>
/// Represents a node in the engine that can render a 3D model.
/// </summary>
[NodeContextMenu]
public class ModelNode : Node, IDisposable
{
    /// <summary>
    /// Gets or sets the model asset associated with this node.
    /// </summary>
    public ModelAsset? ModelAsset { get; set; }

    private Model? model;
    private Vulkan vk = null!;
    private bool modelRead;

    /// <summary>
    /// Gets the model associated with this node. Lazy-loaded from the <see cref="ModelAsset"/>.
    /// </summary>
    [JsonIgnore]
    public Model? Model
    {
        get
        {
            if (model is null && ModelAsset is not null && !modelRead)
            {
                modelRead = true;
                model = ModelAsset.GetContent(vk);
            }
            return model;
        }
        protected set
        {
            model = value;
            modelRead = false;
        }
    }

    /// <summary>
    /// Initializes the ModelNode with the specified parent and Vulkan instance.
    /// </summary>
    /// <param name="parent">The parent node, if any.</param>
    /// <param name="vulkan">The Vulkan instance used for rendering.</param>
    public override void Initialize(Node? parent, Vulkan vulkan)
    {
        base.Initialize(parent, vulkan);

        vk = vulkan;
    }

    /// <summary>
    /// Disposes of the model associated with this node.
    /// </summary>
    public void Dispose()
    {
        model?.Dispose();

        GC.SuppressFinalize(this);
    }
}
