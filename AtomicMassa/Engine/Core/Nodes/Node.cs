using System.Collections.ObjectModel;
using System.ComponentModel;

namespace AtomicMassa.Engine.Core.Nodes;

/// <summary>
/// Represents a node in the engine's scene graph hierarchy.
/// </summary>
[NodeContextMenu]
public class Node : AmObject
{
    /// <summary>
    /// Event that is triggered when the global transform of the node is invalidated.
    /// </summary>
    public event Action? OnGlobalTransformInvalidated;

    /// <summary>
    /// Gets or sets a value indicating whether the node is active.
    /// </summary>
    public bool IsActive { get; set; } = true;

    /// <summary>
    /// Gets or sets the name of the node.
    /// </summary>
    public string Name { get; set; } = "Node";

    private Node? parent;

    /// <summary>
    /// Gets or sets the parent node of this node.
    /// </summary>
    [JsonIgnore, HideInEditor]
    public Node? Parent
    {
        get => parent;
        set
        {
            if (parent != null)
            {
                parent.OnGlobalTransformInvalidated -= InvalidateGlobalTransformCache;
            }

            parent = value;

            if (parent != null)
            {
                parent.OnGlobalTransformInvalidated += InvalidateGlobalTransformCache;
            }

            InvalidateGlobalTransformCache();
        }
    }

    /// <summary>
    /// Gets or sets the list of child nodes.
    /// </summary>
    [HideInEditor]
    public ObservableCollection<Node> Children { get; set; } = [];

    /// <summary>
    /// Gets or sets the list of components attached to this node.
    /// </summary>
    public Collection<Component> Components { get; } = [];

    private Transform transform = new();

    /// <summary>
    /// Gets or sets the transformation data for this node.
    /// </summary>
    public Transform Transform
    {
        get => transform;
        set
        {
            if (transform != value)
            {
                transform.PropertyChanged -= TransformChanged;
                transform = value;
                transform.PropertyChanged += TransformChanged;

                InvalidateGlobalTransformCache();
            }
        }
    }

    private void TransformChanged(object? sender, PropertyChangedEventArgs? e)
    {
        InvalidateGlobalTransformCache();
    }

    private readonly object lockObject = new();

    private Transform? cachedGlobalTransform;

    /// <summary>
    /// Gets the cached global transformation of the node.
    /// </summary>
    [JsonIgnore, HideInEditor]
    public Transform GlobalTransform
    {
        get
        {
            lock (lockObject)
            {
                cachedGlobalTransform ??= CalculateGlobalTransform();
                return cachedGlobalTransform;
            }
        }
        set => Transform = Transform.RemoveParent(value);
    }

    /// <summary>
    /// Initializes the node with the specified parent and Vulkan instance.
    /// </summary>
    /// <param name="parentNew">The parent node, if any.</param>
    /// <param name="vulkan">The Vulkan instance used for rendering.</param>
    public virtual void Initialize(Node? parentNew, Vulkan vulkan)
    {
        Parent = parentNew;
        foreach (var component in Components)
        {
            component.Setup(this);
        }
        foreach (var child in Children)
        {
            child.Initialize(this, vulkan);
        }
    }

    /// <summary>
    /// Loads a node from a file and returns it.
    /// </summary>
    /// <param name="absolutePath">The path to the JSON file containing the node data.</param>
    /// <param name="vulkan">The Vulkan instance to use.</param>
    /// <returns>The loaded node.</returns>
    /// <exception cref="Exception">Thrown when node loading fails.</exception>
    public static Node Load(string absolutePath, Vulkan? vulkan)
    {
        if (File.Exists(absolutePath))
        {
            var node = Load<Node>(absolutePath);

            if (node is not null)
            {
                if (vulkan is not null)
                {
                    node.Initialize(null, vulkan);
                }
                return node;
            }
        }
        throw new InvalidOperationException($"Node load failed {absolutePath}");
    }

    /// <summary>
    /// Retrieves an enumerable collection of child nodes of a specific type.
    /// </summary>
    /// <typeparam name="T">The type of child nodes to retrieve.</typeparam>
    /// <returns>An enumerable collection of child nodes of the specified type.</returns>
    public IEnumerable<T> GetChildren<T>()
        where T : Node
    {
        return GetChildren<T>(this);
    }

    /// <summary>
    /// Retrieves an enumerable collection of child nodes of a specific type.
    /// </summary>
    /// <typeparam name="T">The type of child nodes to retrieve.</typeparam>
    /// <param name="node">The parent node.</param>
    /// <returns>An enumerable collection of child nodes of the specified type.</returns>
    public static IEnumerable<T> GetChildren<T>(Node? node)
        where T : Node
    {
        if (node is null)
        {
            yield break;
        }

        foreach (var child in node.Children)
        {
            if (child.IsActive && child is T t)
            {
                yield return t;
            }

            foreach (var grandChild in GetChildren<T>(child))
            {
                yield return grandChild;
            }
        }
    }

    /// <summary>
    /// Retrieves an enumerable collection of child nodes.
    /// </summary>
    /// <param name="node">The parent node.</param>
    /// <returns>An enumerable collection of child nodes of the specified type.</returns>
    public static IEnumerable<Node> GetChildren(Node? node)
    {
        if (node is null)
        {
            yield break;
        }

        foreach (var child in node.Children)
        {
            if (child.IsActive)
            {
                yield return child;
            }

            foreach (var grandChild in GetChildren(child))
            {
                yield return grandChild;
            }
        }
    }

    /// <summary>
    /// Retrieves an enumerable collection of nodes of a specific type from a list of nodes.
    /// </summary>
    /// <typeparam name="T">The type of nodes to retrieve.</typeparam>
    /// <param name="list">The list of nodes to search.</param>
    /// <returns>An enumerable collection of nodes of the specified type.</returns>
    public static IEnumerable<T> GetTypeAndChildren<T>(IEnumerable<Node>? list)
        where T : Node
    {
        if (list is null)
        {
            yield break;
        }

        foreach (var go in list)
        {
            if (go.IsActive && go is T t)
            {
                yield return t;
            }

            foreach (var children in GetChildren<T>(go))
            {
                yield return children;
            }
        }
    }

    /// <summary>
    /// Retrieves an enumerable collection of nodes of a specific type from a list of nodes.
    /// </summary>
    /// <typeparam name="T">The type of nodes to retrieve.</typeparam>
    /// <param name="list">The list of nodes to search.</param>
    /// <returns>An enumerable collection of nodes of the specified type.</returns>
    private static IEnumerable<T> Get<T>(IEnumerable<Node> list)
        where T : Node
    {
        foreach (var go in list)
        {
            if (go.IsActive && go is T t)
            {
                yield return t;
            }
        }
    }

    /// <summary>
    /// Gets the component of type <typeparamref name="T"/> attached to this node.
    /// </summary>
    /// <typeparam name="T">The type of component to retrieve.</typeparam>
    /// <returns>The component of the specified type, or null if not found.</returns>
    public T? GetComponent<T>()
        where T : Component
    {
        foreach (var component in Components)
        {
            if (component is T componentT)
            {
                return componentT;
            }
        }
        return null;
    }

    /// <summary>
    /// Adds a component to this node.
    /// </summary>
    /// <param name="component">The component to add.</param>
    /// <returns>The added component.</returns>
    /// <exception cref="ArgumentNullException">Thrown if <paramref name="component"/> is null.</exception>
    public Component AddComponent(Component component)
    {
        ArgumentNullException.ThrowIfNull(component);

        // Set up the component with this node as its owner.
        component.Setup(this);

        // Add the component to the list of components.
        Components.Add(component);

        return component;
    }

    /// <summary>
    /// Calculates and returns the global transformation of the node, taking into account its parent's transformation.
    /// </summary>
    /// <returns>The global transformation of the node.</returns>
    private Transform CalculateGlobalTransform()
    {
        // If this node has a parent, combine its transformation with its own.
        return Parent is not null ? Transform.AddParent(Parent.GlobalTransform) : Transform;
    }

    /// <summary>
    /// Invalidates the cached global transform of the node and invokes the global transform invalidated event.
    /// </summary>
    public void InvalidateGlobalTransformCache()
    {
        lock (lockObject)
        {
            // Reset the cached global transform to null.
            cachedGlobalTransform = null;

            // Invoke the global transform invalidated event, if any subscribers.
            OnGlobalTransformInvalidated?.Invoke();
        }
    }

    /// <summary>
    /// Called when the node needs to perform an update.
    /// </summary>
    /// <param name="deltaTime">The time elapsed since the last update.</param>
    public virtual void OnUpdate(float deltaTime) { }
}
