namespace AtomicMassa.Engine.Core.Graphics;

/// <summary>
/// Represents the Global Uniform Buffer Object (UBO) used for global shader data.
/// </summary>
public class GlobalUbo
{
    private const uint uboLights = 10;

    private Matrix4X4<float> projection; // 64
    private Matrix4X4<float> view; // 64
    private Vector4D<float> frontVec; // 16
    private Vector4D<float> ambientColor; // 16

    // size = 160

    private readonly PointLight[] pointLights = null!;

    // size = 10 * 32 * 320

    /// <summary>
    /// Gets the number of point lights in the UBO.
    /// </summary>
    public int Count => pointLights.Length;

    /// <summary>
    /// Initializes a new instance of the <see cref="GlobalUbo"/> class with default values.
    /// </summary>
    public GlobalUbo()
    {
        projection = Matrix4X4<float>.Identity;
        view = Matrix4X4<float>.Identity;
        frontVec = Vector4D<float>.UnitZ;
        ambientColor = new(1f, 1f, 1f, 0.02f);
        pointLights = new PointLight[uboLights];
    }

    /// <summary>
    /// Updates the UBO with new projection, view, and front vector data.
    /// </summary>
    /// <param name="projection">The projection matrix.</param>
    /// <param name="view">The view matrix.</param>
    /// <param name="frontVec">The front vector.</param>
    public void Update(Matrix4X4<float> projection, Matrix4X4<float> view, Vector4D<float> frontVec)
    {
        this.projection = projection;
        this.view = view;
        this.frontVec = frontVec;
    }

    /// <summary>
    /// Sets the position of a point light at the specified index.
    /// </summary>
    /// <param name="lightIndex">The index of the point light.</param>
    /// <param name="position">The position to set.</param>
    public void SetPointLightPosition(int lightIndex, Vector3D<float> position) =>
        pointLights[lightIndex].SetPosition(position);

    /// <summary>
    /// Sets the color and intensity of a point light at the specified index.
    /// </summary>
    /// <param name="lightIndex">The index of the point light.</param>
    /// <param name="color">The color to set.</param>
    /// <param name="intensity">The intensity to set.</param>
    public void SetPointLightColor(int lightIndex, Vector4D<float> color, float intensity) =>
        pointLights[lightIndex].SetColor(color, intensity);

    /// <summary>
    /// Converts the UBO data to a byte array.
    /// </summary>
    /// <returns>A byte array containing the UBO data.</returns>
    public byte[] AsBytes()
    {
        uint offset = 0;
        uint fsize = sizeof(float);
        var vsize = fsize * 4;
        var msize = vsize * 4;
        var bytes = new byte[SizeOf];

        projection.AsBytes().CopyTo(bytes, offset);
        offset += msize;
        view.AsBytes().CopyTo(bytes, offset);
        offset += msize;

        frontVec.AsBytes().CopyTo(bytes, offset);
        offset += vsize;
        ambientColor.AsBytes().CopyTo(bytes, offset);
        offset += vsize;

        var pbytes = pointLights.AsBytes();
        pbytes.CopyTo(bytes, offset);

        return bytes;
    }

    /// <summary>
    /// Gets the size, in bytes, of the UBO.
    /// </summary>
    /// <returns>The size of the UBO in bytes.</returns>
    public uint SizeOf => 160 + (uboLights * 32);
}
