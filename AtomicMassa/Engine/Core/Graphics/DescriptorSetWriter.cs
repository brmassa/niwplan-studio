namespace AtomicMassa.Engine.Core.Graphics;

/// <summary>
/// Helper class for writing descriptor sets used for Vulkan rendering.
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="DescriptorSetWriter"/> class.
/// </remarks>
/// <param name="vk">The Vulkan instance.</param>
/// <param name="device">The Vulkan device.</param>
/// <param name="setLayout">The descriptor set layout to write to.</param>
public unsafe class DescriptorSetWriter(Vk vk, Device device, DescriptorSetLayout setLayout)
{
    private readonly Vk vk = vk;

    private readonly Device device = device;

    private readonly DescriptorSetLayout setLayout = setLayout;

    private readonly List<WriteDescriptorSet> writes = [];

    /// <summary>
    /// Writes a buffer descriptor to the descriptor set.
    /// </summary>
    /// <param name="binding">The binding point within the descriptor set layout.</param>
    /// <param name="bufferInfo">The descriptor buffer information to write.</param>
    /// <returns>The <see cref="DescriptorSetWriter"/> instance.</returns>
    public DescriptorSetWriter WriteBuffer(uint binding, DescriptorBufferInfo bufferInfo)
    {
        ValidateBinding(binding);

        var bindingDescription = setLayout.Bindings[binding];
        ValidateDescriptorCount(bindingDescription);

        writes.Add(
            new WriteDescriptorSet
            {
                SType = StructureType.WriteDescriptorSet,
                DescriptorType = bindingDescription.DescriptorType,
                DstBinding = binding,
                PBufferInfo = &bufferInfo,
                DescriptorCount = 1,
            }
        );

        return this;
    }

    /// <summary>
    /// Writes an image descriptor to the descriptor set.
    /// </summary>
    /// <param name="binding">The binding point within the descriptor set layout.</param>
    /// <param name="imageInfo">The descriptor image information to write.</param>
    /// <returns>The <see cref="DescriptorSetWriter"/> instance.</returns>
    public DescriptorSetWriter WriteImage(uint binding, DescriptorImageInfo imageInfo)
    {
        ValidateBinding(binding);

        var bindingDescription = setLayout.Bindings[binding];
        ValidateDescriptorCount(bindingDescription);

        writes.Add(
            new WriteDescriptorSet
            {
                SType = StructureType.WriteDescriptorSet,
                DescriptorType = bindingDescription.DescriptorType,
                DstBinding = binding,
                PImageInfo = &imageInfo,
                DescriptorCount = 1,
            }
        );

        return this;
    }

    /// <summary>
    /// Builds and updates a descriptor set.
    /// </summary>
    /// <param name="pool">The descriptor pool used for allocation.</param>
    /// <param name="layout">The descriptor set layout.</param>
    /// <param name="set">The descriptor set to update and allocate.</param>
    /// <returns>True if the descriptor set was successfully updated and allocated, otherwise false.</returns>
    public bool Build(
        DescriptorPool pool,
        Silk.NET.Vulkan.DescriptorSetLayout layout,
        ref DescriptorSet set
    )
    {
        ArgumentNullException.ThrowIfNull(pool);

        if (!pool.AllocateDescriptorSet(setLayout.GetDescriptorSetLayout(), ref set))
        {
            return false;
        }

        Overwrite(ref set);
        return true;
    }

    private void Overwrite(ref DescriptorSet set)
    {
        for (var i = 0; i < writes.Count; i++)
        {
            var write = writes[i];
            write.DstSet = set;
            writes[i] = write;
        }

        fixed (WriteDescriptorSet* writesPtr = writes.ToArray())
        {
            vk.UpdateDescriptorSets(device.VkDevice, (uint)writes.Count, writesPtr, 0, null);
        }
    }

    private void ValidateBinding(uint binding)
    {
        if (!setLayout.Bindings.ContainsKey(binding))
        {
            throw new KeyNotFoundException(
                "Layout does not contain the specified binding at {binding}"
            );
        }
    }

    private static void ValidateDescriptorCount(DescriptorSetLayoutBinding bindingDescription)
    {
        if (bindingDescription.DescriptorCount > 1)
        {
            throw new KeyNotFoundException(
                "Binding single descriptor info, but binding expects multiple"
            );
        }
    }
}
