namespace AtomicMassa.Engine.Core.Graphics;

/// <summary>
/// Represents a Vulkan graphics context and device manager.
/// </summary>
public class Vulkan
{
    /// <summary>
    /// Gets the Vulkan API instance.
    /// </summary>
    public Vk Vk { get; init; }

    /// <summary>
    /// Gets the Vulkan device.
    /// </summary>
    public Device Device { get; init; }

    /// <summary>
    /// Initializes a new instance of the <see cref="Vulkan"/> class with the specified window manager and logger.
    /// </summary>
    /// <param name="windowManager">The window manager used for creating the Vulkan device.</param>
    /// <param name="logger">The logger used for reporting startup progress.</param>
    /// <exception cref="ArgumentNullException">Thrown if <paramref name="windowManager"/> is <c>null</c>.</exception>
    public Vulkan(WindowManager windowManager, ILogger logger)
    {
        ArgumentNullException.ThrowIfNull(windowManager);

        Vk = Vk.GetApi();
        Device = new Device(Vk, windowManager.Window);

        logger.Lap("startup", "got vk");
    }
}
