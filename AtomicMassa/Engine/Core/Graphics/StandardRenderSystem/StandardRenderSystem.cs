namespace AtomicMassa.Engine.Core.Graphics.StandardRenderSystem;

/// <summary>
/// StandardPipeline Renderer can draw solids
/// </summary>
public class StandardRenderSystem : IRenderSystem, IDisposable
{
    private readonly Vulkan vulkan;
    private const string vertShaderPath = "standardShader.vert.spv";
    private const string fragShaderPath = "standardShader.frag.spv";

    private StandardPipeline pipeline = null!;
    private PipelineLayout pipelineLayout;

    /// <summary>
    /// .ctor
    /// </summary>
    /// <param name="vulkan"></param>
    /// <param name="renderPass"></param>
    /// <param name="globalSetLayout"></param>
    public StandardRenderSystem(
        Vulkan vulkan,
        RenderPass renderPass,
        Silk.NET.Vulkan.DescriptorSetLayout globalSetLayout
    )
    {
        this.vulkan = vulkan;
        CreatePipelineLayout(globalSetLayout);
        CreatePipeline(renderPass);
    }

    /// <inheritdoc />
    public unsafe void Render(FrameInfo frameInfo, ref GlobalUbo ubo)
    {
        ArgumentNullException.ThrowIfNull(ubo);

        pipeline?.Bind(frameInfo.CommandBuffer);

        vulkan.Vk.CmdBindDescriptorSets(
            frameInfo.CommandBuffer,
            PipelineBindPoint.Graphics,
            pipelineLayout,
            0,
            1,
            frameInfo.GlobalDescriptorSet,
            0,
            null
        );

        UpdateLights(frameInfo, ubo);
        DrawSolids(frameInfo);
    }

    private unsafe void DrawSolids(FrameInfo frameInfo)
    {
        foreach (var go in Node.GetTypeAndChildren<ModelNode>(frameInfo.Nodes))
        {
            if (go.Model is null)
            {
                continue;
            }
            StandardPushConstantData push =
                new()
                {
                    ModelMatrix = go.GlobalTransform.Matrix4X4(),
                    NormalMatrix = go.GlobalTransform.NormalMatrix()
                };
            vulkan.Vk.CmdPushConstants(
                frameInfo.CommandBuffer,
                pipelineLayout,
                ShaderStageFlags.VertexBit | ShaderStageFlags.FragmentBit,
                0,
                StandardPushConstantData.SizeOf(),
                ref push
            );
            go.Model.Bind(frameInfo.CommandBuffer);
            go.Model.Draw(frameInfo.CommandBuffer);
        }
    }

    private static unsafe void UpdateLights(FrameInfo frameInfo, GlobalUbo ubo)
    {
        var lightNodes = Node.GetTypeAndChildren<LightNode>(frameInfo.Nodes).ToList();

        var lightIndex = 0;
        foreach (var lightNode in lightNodes)
        {
            if (lightIndex == ubo.Count)
            {
                break;
            }

            ubo.SetPointLightPosition(lightIndex, lightNode.GlobalTransform.Position);
            ubo.SetPointLightColor(lightIndex, lightNode.Color, lightNode.Intensity);
            lightIndex++;
        }
    }

    /// <inheritdoc />
    public unsafe void Dispose()
    {
        pipeline?.Dispose();
        vulkan.Vk.DestroyPipelineLayout(vulkan.Device.VkDevice, pipelineLayout, null);
        GC.SuppressFinalize(this);
    }

    private unsafe void CreatePipelineLayout(Silk.NET.Vulkan.DescriptorSetLayout globalSetLayout)
    {
        var descriptorSetLayouts = new Silk.NET.Vulkan.DescriptorSetLayout[] { globalSetLayout };
        PushConstantRange pushConstantRange = new()
        {
            StageFlags = ShaderStageFlags.VertexBit | ShaderStageFlags.FragmentBit,
            Offset = 0,
            Size = StandardPushConstantData.SizeOf(),
        };

        fixed (Silk.NET.Vulkan.DescriptorSetLayout* descriptorSetLayoutPtr = descriptorSetLayouts)
        {
            PipelineLayoutCreateInfo pipelineLayoutInfo = new()
            {
                SType = StructureType.PipelineLayoutCreateInfo,
                SetLayoutCount = (uint)descriptorSetLayouts.Length,
                PSetLayouts = descriptorSetLayoutPtr,
                PushConstantRangeCount = 1,
                PPushConstantRanges = &pushConstantRange,
            };

            var resultVulkan = vulkan.Vk.CreatePipelineLayout(vulkan.Device.VkDevice, pipelineLayoutInfo, null, out pipelineLayout);
            if (resultVulkan != Result.Success)
            {
                throw new VulkanException("Vulkan: failed to create pipeline layout: {0}", resultVulkan);
            }
        }
    }

    private void CreatePipeline(RenderPass renderPass)
    {
        Debug.Assert(pipelineLayout.Handle != 0, "Cannot create pipeline before pipeline layout");

        var pipelineConfig = new PipelineConfigInfo();
        StandardPipeline.DefaultPipelineConfigInfo(ref pipelineConfig);

        StandardPipeline.EnableMultiSampling(ref pipelineConfig, vulkan.Device.MsaaSamples);

        pipelineConfig.RenderPass = renderPass;
        pipelineConfig.PipelineLayout = pipelineLayout;
        pipeline = new StandardPipeline(
            vulkan.Vk,
            vulkan.Device,
            vertShaderPath,
            fragShaderPath,
            pipelineConfig
        );
    }
}
