namespace AtomicMassa.Engine.Core.Graphics;

/// <summary>
/// Specific Exception type for Vulkan related issues.
/// </summary>
/// <remarks>
/// .ctor
/// </remarks>
/// <param name="message"></param>
/// <param name="additionalInfo"></param>
public class VulkanException(string? message = null, object? additionalInfo = null) : Exception(FormatMessage(message, additionalInfo))
{
    private static string FormatMessage(string? message, object? additionalInfo)
    {
        message ??= "Vulkan: An error occurred. {0}";
        if (additionalInfo == null || additionalInfo is string additionalInfoStr && string.IsNullOrEmpty(additionalInfoStr))
        {
            return message;
        }
        Log.Error(message, additionalInfo);

        return string.Format(CultureInfo.InvariantCulture, message, additionalInfo);
    }
}
