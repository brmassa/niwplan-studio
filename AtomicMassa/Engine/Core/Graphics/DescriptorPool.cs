namespace AtomicMassa.Engine.Core.Graphics;

/// <summary>
/// Represents a descriptor pool in Vulkan, managing the allocation and deallocation of descriptor sets.
/// </summary>
public unsafe class DescriptorPool : IDisposable
{
    /// <summary>
    /// Gets the Device object, representing the GPU or similar device being used.
    /// </summary>
    public Device LveDevice { get; }

    /// <summary>
    /// Gets the descriptor pool handle.
    /// </summary>
    public Silk.NET.Vulkan.DescriptorPool GetDescriptorPool() => descriptorPool;

    /// <summary>
    /// Reference to Vulkan API object.
    /// </summary>
    private readonly Vk vk = null!;

    private Silk.NET.Vulkan.DescriptorPool descriptorPool;

    private readonly DescriptorPoolSize[] poolSizes;

    // private readonly DescriptorPoolCreateFlags poolFlags; // = DescriptorPoolCreateFlags.None;

    private readonly uint maxSets;

    /// <summary>
    /// Initializes a new instance of the DescriptorPool class.
    /// </summary>
    /// <param name="vk">The Vulkan API object.</param>
    /// <param name="device">The device object.</param>
    /// <param name="maxSets">The maximum number of descriptor sets that can be allocated from the pool.</param>
    /// <param name="poolFlags">The descriptor pool creation flags.</param>
    /// <param name="poolSizes">Array of descriptor pool sizes.</param>
    public DescriptorPool(
        Vk vk,
        Device device,
        uint maxSets,
        DescriptorPoolCreateFlags poolFlags,
        DescriptorPoolSize[] poolSizes
    )
    {
        ArgumentNullException.ThrowIfNull(vk);
        ArgumentNullException.ThrowIfNull(device);
        ArgumentNullException.ThrowIfNull(poolSizes);
        this.vk = vk;
        this.LveDevice = device;
        this.poolSizes = poolSizes;
        this.maxSets = maxSets;
        // this.poolFlags = poolFlags;

        fixed (Silk.NET.Vulkan.DescriptorPool* descriptorPoolPtr = &descriptorPool)
        fixed (DescriptorPoolSize* poolSizesPtr = poolSizes)
        {
            DescriptorPoolCreateInfo descriptorPoolInfo =
                new()
                {
                    SType = StructureType.DescriptorPoolCreateInfo,
                    PoolSizeCount = (uint)poolSizes.Length,
                    PPoolSizes = poolSizesPtr,
                    MaxSets = maxSets,
                    Flags = poolFlags
                };


            var resultVulkan = vk.CreateDescriptorPool(device.VkDevice, &descriptorPoolInfo, null, descriptorPoolPtr);
            if (resultVulkan != Result.Success)
            {
                throw new VulkanException("Vulkan: failed to create descriptor pool: {0}", resultVulkan);
            }
        }
    }

    /// <summary>
    /// Allocate a descriptor set from the pool.
    /// </summary>
    /// <param name="descriptorSetLayout">The layout of the descriptor set to allocate.</param>
    /// <param name="descriptorSet">Reference to store the allocated descriptor set.</param>
    /// <returns>True if allocation was successful, false otherwise.</returns>
    public bool AllocateDescriptorSet(
        Silk.NET.Vulkan.DescriptorSetLayout descriptorSetLayout,
        ref DescriptorSet descriptorSet
    )
    {
        var allocInfo = new DescriptorSetAllocateInfo()
        {
            SType = StructureType.DescriptorSetAllocateInfo,
            DescriptorPool = descriptorPool,
            PSetLayouts = &descriptorSetLayout,
            DescriptorSetCount = 1,
        };
        var result = vk.AllocateDescriptorSets(LveDevice.VkDevice, allocInfo, out descriptorSet);

        return result == Result.Success;
    }

    /// <summary>
    /// Releases all resources used by the DescriptorPool object.
    /// </summary>
    public void Dispose()
    {
        vk.DestroyDescriptorPool(LveDevice.VkDevice, descriptorPool, null);
        GC.SuppressFinalize(this);
    }

    private void FreeDescriptors(ref DescriptorSet[] descriptors)
    {
        _ = vk.FreeDescriptorSets(LveDevice.VkDevice, descriptorPool, descriptors);
    }

    private void ResetPool()
    {
        _ = vk.ResetDescriptorPool(LveDevice.VkDevice, descriptorPool, 0);
    }
}
