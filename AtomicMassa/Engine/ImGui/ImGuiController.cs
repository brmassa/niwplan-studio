using System.Numerics;

namespace AtomicMassa.Engine.ImGui;

/// <summary>
/// Control the render of IMGUI widgets
/// </summary>
public class ImGuiController : IDisposable
{
    private readonly Vk vk = null!;
    private readonly IView view = null!;
    private readonly IInputContext input = null!;
    private Silk.NET.Vulkan.Device device;
    private PhysicalDevice physicalDevice;
    private bool frameBegun;
    private readonly List<char> pressedChars = [];
    private IKeyboard keyboard = null!;
    private Silk.NET.Vulkan.DescriptorPool descriptorPool;
    private RenderPass renderPass;
    private int windowWidth;
    private int windowHeight;
    private readonly int swapChainImageCt;
    private Sampler fontSampler;
    private Silk.NET.Vulkan.DescriptorSetLayout descriptorSetLayout;
    private DescriptorSet descriptorSet;
    private PipelineLayout pipelineLayout;
    private ShaderModule shaderModuleVert;
    private ShaderModule shaderModuleFrag;
    private Pipeline pipeline;
    private WindowRenderBuffers mainWindowRenderBuffers;
    private GlobalMemory frameRenderBuffers = null!;
    private DeviceMemory fontMemory;
    private Image fontImage;
    private ImageView fontView;
    private ulong bufferMemoryAlignment = 256;
    private SwapChain swapChain = null!;
    private readonly ImGuiIOPtr io;

    /// <summary>
    /// Constructs a new ImGuiController.
    /// </summary>
    /// <param name="vk">The vulkan api instance</param>
    /// <param name="view">Window view</param>
    /// <param name="input">Input context</param>
    /// <param name="physicalDevice">The physical device instance in use</param>
    /// <param name="graphicsFamilyIndex">The graphics family index corresponding to the graphics queue</param>
    /// <param name="swapChainImageCt">The number of images used in the swap chain</param>
    /// <param name="swapChainFormat">The image format used by the swap chain</param>
    /// <param name="depthBufferFormat">The image formate used by the depth buffer, or null if no depth buffer is used</param>
    /// <param name="msaaSamples"></param>
    public unsafe ImGuiController(
        Vk vk,
        IView view,
        IInputContext input,
        PhysicalDevice physicalDevice,
        uint graphicsFamilyIndex,
        int swapChainImageCt,
        Format swapChainFormat,
        Format? depthBufferFormat,
        SampleCountFlags msaaSamples
    )
    {
        ArgumentNullException.ThrowIfNull(vk);
        ArgumentNullException.ThrowIfNull(view);

        var context = ImGuiNET.ImGui.CreateContext();
        ImGuiNET.ImGui.SetCurrentContext(context);
        this.vk = vk;
        this.view = view;
        this.input = input;
        this.physicalDevice = physicalDevice;
        windowWidth = view.Size.X;
        windowHeight = view.Size.Y;
        this.swapChainImageCt = swapChainImageCt;

        if (swapChainImageCt < 2)
        {
            throw new ArgumentException("Swap chain image count must be >= 2");
        }

        if (!this.vk.CurrentDevice.HasValue)
        {
            throw new InvalidOperationException(
                "vk.CurrentDevice is null. _vk.CurrentDevice must be set to the current device."
            );
        }

        device = this.vk.CurrentDevice.Value;

        // Set default style
        ImGuiNET.ImGui.StyleColorsDark();

        InitializeCreateDescriptorPool();

        // Create the render pass
        InitializeCreateRenderPass(vk, swapChainFormat, depthBufferFormat, msaaSamples);
        InitializeCreateSampler(vk);
        InitializeCreateDescriptorSetLayout(vk);
        InitializeAllocateDescriptorSets(vk);
        InitializeCreatePipelineLayout(vk);

        // Create the shader modules
        InitializeShaderModules(vk);

        // Create the pipeline
        var stage = InitalizePipeline(vk, msaaSamples);

        _ = SilkMarshal.Free((nint)stage[0].PName);
        _ = SilkMarshal.Free((nint)stage[1].PName);

        // Initialise ImGui Vulkan adapter
        io = ImGuiNET.ImGui.GetIO();
        io.BackendFlags |= ImGuiBackendFlags.RendererHasVtxOffset;
        io.Fonts.GetTexDataAsRGBA32(out nint pixels, out var width, out var height);
        var uploadSize = (ulong)(width * height * 4 * sizeof(byte));

        // Submit one-time command to create the fonts texture

        var commandPool = InitializeCommandPool(graphicsFamilyIndex);

        var commandBuffer = InitializeCommandBuffer(commandPool);

        InitializeBeginInfo(commandBuffer);

        InitializeImageInfo(width, height);

        this.vk.GetImageMemoryRequirements(device, fontImage, out var fontReq);

        Initialize6(vk, uploadSize, fontReq, out var uploadBuffer, out var uploadReq);

        bufferMemoryAlignment =
            (bufferMemoryAlignment > uploadReq.Alignment)
                ? bufferMemoryAlignment
                : uploadReq.Alignment;

        var uploadBufferMemory = Initialize1(vk, pixels, uploadSize, uploadBuffer, uploadReq);

        Initialize4(width, height, commandBuffer, uploadBuffer);

        // Store our identifier
        io.Fonts.SetTexID((IntPtr)fontImage.Handle);

        this.vk.GetDeviceQueue(device, graphicsFamilyIndex, 0, out var graphicsQueue);

        Initialize1QueueWaitIdle(commandBuffer, graphicsQueue);

        this.vk.DestroyBuffer(device, uploadBuffer, default);
        this.vk.FreeMemory(device, uploadBufferMemory, default);
        this.vk.DestroyCommandPool(device, commandPool, default);

        SetKeyMappings();

        SetPerFrameImGuiData(1f / 60f);

        BeginFrame();
    }

    private unsafe CommandPool InitializeCommandPool(uint graphicsFamilyIndex)
    {
        var poolInfo = new CommandPoolCreateInfo
        {
            SType = StructureType.CommandPoolCreateInfo,
            QueueFamilyIndex = graphicsFamilyIndex
        };

        var resultVulkan = vk.CreateCommandPool(device, poolInfo, null, out var commandPool);
        if (resultVulkan != Result.Success)
        {
            throw new VulkanException("Vulkan: failed to create command pool: {0}", resultVulkan);
        }

        return commandPool;
    }

    private unsafe CommandBuffer InitializeCommandBuffer(CommandPool commandPool)
    {
        var allocInfo = new CommandBufferAllocateInfo
        {
            SType = StructureType.CommandBufferAllocateInfo,
            CommandPool = commandPool,
            Level = CommandBufferLevel.Primary,
            CommandBufferCount = 1
        };
        if (
            this.vk.AllocateCommandBuffers(device, allocInfo, out var commandBuffer)
            != Result.Success
        )
        {
            throw new VulkanException("Unable to allocate command buffers");
        }

        return commandBuffer;
    }

    private unsafe void Initialize1QueueWaitIdle(CommandBuffer commandBuffer, Queue graphicsQueue)
    {
        var resultVulkan = vk.EndCommandBuffer(commandBuffer);
        if (resultVulkan != Result.Success)
        {
            throw new VulkanException("Vulkan: failed to begin a command buffer: {0}", resultVulkan);
        }

        var submitInfo = new SubmitInfo
        {
            SType = StructureType.SubmitInfo,
            CommandBufferCount = 1,
            PCommandBuffers = (CommandBuffer*)Unsafe.AsPointer(ref commandBuffer)
        };

        resultVulkan = vk.QueueSubmit(graphicsQueue, 1, submitInfo, default);
        if (resultVulkan != Result.Success)
        {
            throw new VulkanException("Vulkan: failed to begin a command buffer: {0}", resultVulkan);
        }

        resultVulkan = vk.QueueWaitIdle(graphicsQueue);
        if (resultVulkan != Result.Success)
        {
            throw new VulkanException("Vulkan: failed to begin a command buffer: {0}", resultVulkan);
        }
    }

    /// <summary>
    /// Renders the ImGui draw list data.
    /// </summary>
    public void Render(
        CommandBuffer commandBuffer,
        Framebuffer framebuffer,
        Extent2D swapChainExtent
    )
    {
        if (frameBegun)
        {
            frameBegun = false;
            ImGuiNET.ImGui.Render();
            RenderImDrawData(
                ImGuiNET.ImGui.GetDrawData(),
                commandBuffer,
                framebuffer,
                swapChainExtent
            );
        }
    }

    /// <summary>
    /// Give the needed parameters that could not be deliveried via constructor
    /// </summary>
    /// <param name="swapChain"></param>
    public void Setup(SwapChain swapChain) => this.swapChain = swapChain;

    /// <summary>
    /// Render the IMGUI
    /// </summary>
    /// <param name="frameInfo"></param>
    /// <param name="_"></param>
    public void Render(FrameInfo frameInfo, ref GlobalUbo _)
    {
        if (frameBegun)
        {
            frameBegun = false;
            ImGuiNET.ImGui.Render();
            RenderImDrawData(
                ImGuiNET.ImGui.GetDrawData(),
                frameInfo.CommandBuffer,
                swapChain.GetFrameBufferAt((uint)frameInfo.FrameIndex),
                swapChain.SwapChainExtent
            );
        }
    }

    /// <summary>
    /// Updates ImGui input and IO configuration state. Call Update() before drawing and rendering.
    /// </summary>
    public void Update(float deltaSeconds)
    {
        if (frameBegun)
        {
            ImGuiNET.ImGui.Render();
        }

        SetPerFrameImGuiData(deltaSeconds);
        UpdateImGuiInput();

        frameBegun = true;
        ImGuiNET.ImGui.NewFrame();
    }

    /// <summary>
    /// Frees all graphics resources used by the renderer.
    /// </summary>
    public unsafe void Dispose()
    {
        view.Resize -= WindowResized;

        for (uint n = 0; n < mainWindowRenderBuffers.Count; n++)
        {
            vk.DestroyBuffer(
                device,
                mainWindowRenderBuffers.FrameRenderBuffers[n].VertexBuffer,
                default
            );
            vk.FreeMemory(
                device,
                mainWindowRenderBuffers.FrameRenderBuffers[n].VertexBufferMemory,
                default
            );
            vk.DestroyBuffer(
                device,
                mainWindowRenderBuffers.FrameRenderBuffers[n].IndexBuffer,
                default
            );
            vk.FreeMemory(
                device,
                mainWindowRenderBuffers.FrameRenderBuffers[n].IndexBufferMemory,
                default
            );
        }

        vk.DestroyShaderModule(device, shaderModuleVert, default);
        vk.DestroyShaderModule(device, shaderModuleFrag, default);
        vk.DestroyImageView(device, fontView, default);
        vk.DestroyImage(device, fontImage, default);
        vk.FreeMemory(device, fontMemory, default);
        vk.DestroySampler(device, fontSampler, default);
        vk.DestroyDescriptorSetLayout(device, descriptorSetLayout, default);
        vk.DestroyPipelineLayout(device, pipelineLayout, default);
        vk.DestroyPipeline(device, pipeline, default);
        if (vk.CurrentDevice.HasValue)
        {
            vk.DestroyDescriptorPool(vk.CurrentDevice.Value, descriptorPool, default);
            vk.DestroyRenderPass(vk.CurrentDevice.Value, renderPass, default);
        }

        ImGuiNET.ImGui.DestroyContext();

        GC.SuppressFinalize(this);
    }

    private unsafe void InitializeCreateDescriptorPool()
    {
        // Create the descriptor pool for ImGui
        Span<DescriptorPoolSize> poolSizes =
            [new DescriptorPoolSize(DescriptorType.CombinedImageSampler, 1)];
        var descriptorPool = new DescriptorPoolCreateInfo
        {
            SType = StructureType.DescriptorPoolCreateInfo,
            PoolSizeCount = (uint)poolSizes.Length,
            PPoolSizes = (DescriptorPoolSize*)
                Unsafe.AsPointer(ref poolSizes.GetPinnableReference()),
            MaxSets = 1
        };
        if (
            this.vk.CreateDescriptorPool(device, descriptorPool, default, out this.descriptorPool)
            != Result.Success
        )
        {
            throw new VulkanException("Unable to create descriptor pool");
        }
    }

    private unsafe void InitializeBeginInfo(CommandBuffer commandBuffer)
    {
        var beginInfo = new CommandBufferBeginInfo
        {
            SType = StructureType.CommandBufferBeginInfo,
            Flags = CommandBufferUsageFlags.OneTimeSubmitBit
        };

        var resultVulkan = vk.BeginCommandBuffer(commandBuffer, beginInfo);
        if (resultVulkan != Result.Success)
        {
            throw new VulkanException("Vulkan: failed to begin a command buffer: {0}", resultVulkan);
        }
    }

    private unsafe void InitializeImageInfo(int width, int height)
    {
        var imageInfo = new ImageCreateInfo
        {
            SType = StructureType.ImageCreateInfo,
            ImageType = ImageType.Type2D,
            Format = Format.R8G8B8A8Unorm,
            MipLevels = 1,
            ArrayLayers = 1,
            Samples = SampleCountFlags.Count1Bit,
            Tiling = ImageTiling.Optimal,
            Usage = ImageUsageFlags.SampledBit | ImageUsageFlags.TransferDstBit,
            SharingMode = SharingMode.Exclusive,
            InitialLayout = ImageLayout.Undefined
        };
        imageInfo.Extent.Width = (uint)width;
        imageInfo.Extent.Height = (uint)height;
        imageInfo.Extent.Depth = 1;

        var resultVulkan = vk.CreateImage(device, imageInfo, default, out fontImage);
        if (resultVulkan != Result.Success)
        {
            throw new VulkanException("Vulkan: failed to create font image: {0}", resultVulkan);
        }
    }

    private unsafe void InitializeShaderModules(Vk vk)
    {
        if (shaderModuleVert.Handle == default)
        {
            fixed (uint* vertShaderBytes = &Shaders.VertexShader[0])
            {
                var vertInfo = new ShaderModuleCreateInfo
                {
                    SType = StructureType.ShaderModuleCreateInfo,
                    CodeSize = (nuint)Shaders.VertexShader.Length * sizeof(uint),
                    PCode = vertShaderBytes
                };
                if (
                    vk.CreateShaderModule(device, vertInfo, default, out shaderModuleVert)
                    != Result.Success
                )
                {
                    throw new VulkanException("Unable to create the vertex shader");
                }
            }
        }
        if (shaderModuleFrag.Handle == default)
        {
            fixed (uint* fragShaderBytes = &Shaders.FragmentShader[0])
            {
                var fragInfo = new ShaderModuleCreateInfo
                {
                    SType = StructureType.ShaderModuleCreateInfo,
                    CodeSize = (nuint)Shaders.FragmentShader.Length * sizeof(uint),
                    PCode = fragShaderBytes
                };
                if (
                    vk.CreateShaderModule(device, fragInfo, default, out shaderModuleFrag)
                    != Result.Success
                )
                {
                    throw new VulkanException("Unable to create the fragment shader");
                }
            }
        }
    }

    private unsafe void Initialize6(
        Vk vk,
        ulong uploadSize,
        MemoryRequirements fontReq,
        out Silk.NET.Vulkan.Buffer uploadBuffer,
        out MemoryRequirements uploadReq
    )
    {
        var fontAllocInfo = new MemoryAllocateInfo
        {
            SType = StructureType.MemoryAllocateInfo,
            AllocationSize = fontReq.Size,
            MemoryTypeIndex = GetMemoryTypeIndex(
                vk,
                MemoryPropertyFlags.DeviceLocalBit,
                fontReq.MemoryTypeBits
            )
        };

        var resultVulkan = this.vk.AllocateMemory(device, &fontAllocInfo, default, out fontMemory);
        if (resultVulkan != Result.Success)
        {
            throw new VulkanException("Vulkan: failed to allocate device memory: {0}", resultVulkan);
        }
        resultVulkan = this.vk.BindImageMemory(device, fontImage, fontMemory, 0);
        if (resultVulkan != Result.Success)
        {
            throw new VulkanException("Vulkan: failed to bind device memory: {0}", resultVulkan);
        }

        var imageViewInfo = new ImageViewCreateInfo
        {
            SType = StructureType.ImageViewCreateInfo,
            Image = fontImage,
            ViewType = ImageViewType.Type2D,
            Format = Format.R8G8B8A8Unorm
        };
        imageViewInfo.SubresourceRange.AspectMask = ImageAspectFlags.ColorBit;
        imageViewInfo.SubresourceRange.LevelCount = 1;
        imageViewInfo.SubresourceRange.LayerCount = 1;

        resultVulkan = vk.CreateImageView(device, &imageViewInfo, default, out fontView);
        if (resultVulkan != Result.Success)
        {
            throw new VulkanException("Vulkan: failed to create an image view: {0}", resultVulkan);
        }

        var descImageInfo = new DescriptorImageInfo
        {
            Sampler = fontSampler,
            ImageView = fontView,
            ImageLayout = ImageLayout.ShaderReadOnlyOptimal
        };
        var writeDescriptors = new WriteDescriptorSet
        {
            SType = StructureType.WriteDescriptorSet,
            DstSet = descriptorSet,
            DescriptorCount = 1,
            DescriptorType = DescriptorType.CombinedImageSampler,
            PImageInfo = (DescriptorImageInfo*)Unsafe.AsPointer(ref descImageInfo)
        };
        this.vk.UpdateDescriptorSets(device, 1, writeDescriptors, 0, default);

        // Create the Upload Buffer:
        var bufferInfo = new BufferCreateInfo
        {
            SType = StructureType.BufferCreateInfo,
            Size = uploadSize,
            Usage = BufferUsageFlags.TransferSrcBit,
            SharingMode = SharingMode.Exclusive
        };

        resultVulkan = vk.CreateBuffer(device, bufferInfo, default, out uploadBuffer);
        if (resultVulkan != Result.Success)
        {
            throw new VulkanException("Vulkan: failed to create a device buffer: {0}", resultVulkan);
        }

        this.vk.GetBufferMemoryRequirements(device, uploadBuffer, out uploadReq);
    }

    private unsafe void InitializeAllocateDescriptorSets(Vk vk)
    {
        fixed (Silk.NET.Vulkan.DescriptorSetLayout* pgDescriptorSetLayout = &descriptorSetLayout)
        {
            var allocInfo = new DescriptorSetAllocateInfo
            {
                SType = StructureType.DescriptorSetAllocateInfo,
                DescriptorPool = this.descriptorPool,
                DescriptorSetCount = 1,
                PSetLayouts = pgDescriptorSetLayout
            };

            var resultVulkan = vk.AllocateDescriptorSets(device, allocInfo, out descriptorSet);
            if (resultVulkan != Result.Success)
            {
                throw new VulkanException("Vulkan: failed to create descriptor sets: {0}", resultVulkan);
            }
        }
    }

    private unsafe void InitializeCreateDescriptorSetLayout(Vk vk)
    {
        var sampler = fontSampler;

        var binding = new DescriptorSetLayoutBinding
        {
            DescriptorType = DescriptorType.CombinedImageSampler,
            DescriptorCount = 1,
            StageFlags = ShaderStageFlags.FragmentBit,
            PImmutableSamplers = (Sampler*)Unsafe.AsPointer(ref sampler)
        };

        var descriptorInfo = new DescriptorSetLayoutCreateInfo
        {
            SType = StructureType.DescriptorSetLayoutCreateInfo,
            BindingCount = 1,
            PBindings = (DescriptorSetLayoutBinding*)Unsafe.AsPointer(ref binding)
        };
        if (
            vk.CreateDescriptorSetLayout(device, descriptorInfo, default, out descriptorSetLayout)
            != Result.Success
        )
        {
            throw new VulkanException("Unable to create descriptor set layout");
        }
    }

    private unsafe void InitializeCreateSampler(Vk vk)
    {
        var info = new SamplerCreateInfo
        {
            SType = StructureType.SamplerCreateInfo,
            MagFilter = Filter.Linear,
            MinFilter = Filter.Linear,
            MipmapMode = SamplerMipmapMode.Linear,
            AddressModeU = SamplerAddressMode.Repeat,
            AddressModeV = SamplerAddressMode.Repeat,
            AddressModeW = SamplerAddressMode.Repeat,
            MinLod = -1000,
            MaxLod = 1000,
            MaxAnisotropy = 1.0f
        };

        var resultVulkan = vk.CreateSampler(device, info, default, out fontSampler);
        if (resultVulkan != Result.Success)
        {
            throw new VulkanException("Vulkan: failed to create sampler: {0}", resultVulkan);
        }
    }

    private unsafe Span<PipelineShaderStageCreateInfo> InitalizePipeline(
        Vk vk,
        SampleCountFlags msaaSamples
    )
    {
        var stage = new PipelineShaderStageCreateInfo[2];
        stage[0].SType = StructureType.PipelineShaderStageCreateInfo;
        stage[0].Stage = ShaderStageFlags.VertexBit;
        stage[0].Module = shaderModuleVert;
        stage[0].PName = (byte*)SilkMarshal.StringToPtr("main");
        stage[1].SType = StructureType.PipelineShaderStageCreateInfo;
        stage[1].Stage = ShaderStageFlags.FragmentBit;
        stage[1].Module = shaderModuleFrag;
        stage[1].PName = (byte*)SilkMarshal.StringToPtr("main");

        var bindingDesc = new VertexInputBindingDescription
        {
            Stride = (uint)Unsafe.SizeOf<ImDrawVert>(),
            InputRate = VertexInputRate.Vertex
        };

        Span<VertexInputAttributeDescription> attributeDesc =
            stackalloc VertexInputAttributeDescription[3];
        attributeDesc[0].Location = 0;
        attributeDesc[0].Binding = bindingDesc.Binding;
        attributeDesc[0].Format = Format.R32G32Sfloat;
        attributeDesc[0].Offset = (uint)Marshal.OffsetOf<ImDrawVert>(nameof(ImDrawVert.pos));
        attributeDesc[1].Location = 1;
        attributeDesc[1].Binding = bindingDesc.Binding;
        attributeDesc[1].Format = Format.R32G32Sfloat;
        attributeDesc[1].Offset = (uint)Marshal.OffsetOf<ImDrawVert>(nameof(ImDrawVert.uv));
        attributeDesc[2].Location = 2;
        attributeDesc[2].Binding = bindingDesc.Binding;
        attributeDesc[2].Format = Format.R8G8B8A8Unorm;
        attributeDesc[2].Offset = (uint)Marshal.OffsetOf<ImDrawVert>(nameof(ImDrawVert.col));

        var vertexInfo = new PipelineVertexInputStateCreateInfo
        {
            SType = StructureType.PipelineVertexInputStateCreateInfo,
            VertexBindingDescriptionCount = 1,
            PVertexBindingDescriptions = (VertexInputBindingDescription*)
                Unsafe.AsPointer(ref bindingDesc),
            VertexAttributeDescriptionCount = 3,
            PVertexAttributeDescriptions = (VertexInputAttributeDescription*)
                Unsafe.AsPointer(ref attributeDesc[0])
        };

        var iaInfo = new PipelineInputAssemblyStateCreateInfo
        {
            SType = StructureType.PipelineInputAssemblyStateCreateInfo,
            Topology = PrimitiveTopology.TriangleList
        };

        var viewportInfo = new PipelineViewportStateCreateInfo
        {
            SType = StructureType.PipelineViewportStateCreateInfo,
            ViewportCount = 1,
            ScissorCount = 1
        };

        var rasterInfo = new PipelineRasterizationStateCreateInfo
        {
            SType = StructureType.PipelineRasterizationStateCreateInfo,
            PolygonMode = PolygonMode.Fill,
            CullMode = CullModeFlags.None,
            FrontFace = FrontFace.CounterClockwise,
            LineWidth = 1.0f
        };

        var msInfo = new PipelineMultisampleStateCreateInfo
        {
            SType = StructureType.PipelineMultisampleStateCreateInfo,
            RasterizationSamples = msaaSamples
        };

        var colorAttachment = new PipelineColorBlendAttachmentState
        {
            BlendEnable = new Silk.NET.Core.Bool32(true),
            SrcColorBlendFactor = BlendFactor.SrcAlpha,
            DstColorBlendFactor = BlendFactor.OneMinusSrcAlpha,
            ColorBlendOp = BlendOp.Add,
            SrcAlphaBlendFactor = BlendFactor.One,
            DstAlphaBlendFactor = BlendFactor.OneMinusSrcAlpha,
            AlphaBlendOp = BlendOp.Add,
            ColorWriteMask =
                ColorComponentFlags.RBit
                | ColorComponentFlags.GBit
                | ColorComponentFlags.BBit
                | ColorComponentFlags.ABit
        };

        var depthInfo = new PipelineDepthStencilStateCreateInfo
        {
            SType = StructureType.PipelineDepthStencilStateCreateInfo
        };

        var blendInfo = new PipelineColorBlendStateCreateInfo
        {
            SType = StructureType.PipelineColorBlendStateCreateInfo,
            AttachmentCount = 1,
            PAttachments = (PipelineColorBlendAttachmentState*)
                Unsafe.AsPointer(ref colorAttachment)
        };

        Span<DynamicState> dynamicStates =
            [DynamicState.Viewport, DynamicState.Scissor];
        var dynamicState = new PipelineDynamicStateCreateInfo
        {
            SType = StructureType.PipelineDynamicStateCreateInfo,
            DynamicStateCount = (uint)dynamicStates.Length,
            PDynamicStates = (DynamicState*)Unsafe.AsPointer(ref dynamicStates[0])
        };

        var pipelineInfo = new GraphicsPipelineCreateInfo
        {
            SType = StructureType.GraphicsPipelineCreateInfo,
            Flags = default,
            StageCount = 2,
            PStages = (PipelineShaderStageCreateInfo*)Unsafe.AsPointer(ref stage[0]),
            PVertexInputState = (PipelineVertexInputStateCreateInfo*)
                Unsafe.AsPointer(ref vertexInfo),
            PInputAssemblyState = (PipelineInputAssemblyStateCreateInfo*)
                Unsafe.AsPointer(ref iaInfo),
            PViewportState = (PipelineViewportStateCreateInfo*)Unsafe.AsPointer(ref viewportInfo),
            PRasterizationState = (PipelineRasterizationStateCreateInfo*)
                Unsafe.AsPointer(ref rasterInfo),
            PMultisampleState = (PipelineMultisampleStateCreateInfo*)Unsafe.AsPointer(ref msInfo),
            PDepthStencilState = (PipelineDepthStencilStateCreateInfo*)
                Unsafe.AsPointer(ref depthInfo),
            PColorBlendState = (PipelineColorBlendStateCreateInfo*)Unsafe.AsPointer(ref blendInfo),
            PDynamicState = (PipelineDynamicStateCreateInfo*)Unsafe.AsPointer(ref dynamicState),
            Layout = pipelineLayout,
            RenderPass = renderPass,
            Subpass = 0
        };
        if (
            vk.CreateGraphicsPipelines(device, default, 1, pipelineInfo, default, out pipeline)
            != Result.Success
        )
        {
            throw new VulkanException("Unable to create the pipeline");
        }

        return stage;
    }

    private unsafe void InitializeCreatePipelineLayout(Vk vk)
    {
        var vertPushConst = new PushConstantRange
        {
            StageFlags = ShaderStageFlags.VertexBit,
            Offset = sizeof(float) * 0,
            Size = sizeof(float) * 4
        };

        var setLayout = descriptorSetLayout;
        var layoutInfo = new PipelineLayoutCreateInfo
        {
            SType = StructureType.PipelineLayoutCreateInfo,
            SetLayoutCount = 1,
            PSetLayouts = (Silk.NET.Vulkan.DescriptorSetLayout*)Unsafe.AsPointer(ref setLayout),
            PushConstantRangeCount = 1,
            PPushConstantRanges = (PushConstantRange*)Unsafe.AsPointer(ref vertPushConst)
        };
        if (
            vk.CreatePipelineLayout(device, layoutInfo, default, out pipelineLayout)
            != Result.Success
        )
        {
            throw new VulkanException("Unable to create the descriptor set layout");
        }
    }

    private unsafe void InitializeCreateRenderPass(
        Vk vk,
        Format swapChainFormat,
        Format? depthBufferFormat,
        SampleCountFlags msaaSamples
    )
    {
        if (depthBufferFormat is null)
        {
            throw new VulkanException("ImGui Depth Format must have a value!");
        }

        AttachmentDescription depthAttachment =
            new()
            {
                Format = depthBufferFormat.Value,
                Samples = msaaSamples,
                LoadOp = AttachmentLoadOp.Clear,
                StoreOp = AttachmentStoreOp.DontCare,
                StencilLoadOp = AttachmentLoadOp.DontCare,
                StencilStoreOp = AttachmentStoreOp.DontCare,
                InitialLayout = ImageLayout.Undefined,
                FinalLayout = ImageLayout.DepthStencilAttachmentOptimal,
            };

        AttachmentReference depthAttachmentRef =
            new() { Attachment = 1, Layout = ImageLayout.DepthStencilAttachmentOptimal, };

        AttachmentDescription colorAttachment =
            new()
            {
                Format = swapChainFormat,
                Samples = msaaSamples,
                LoadOp = AttachmentLoadOp.Clear,
                StoreOp = AttachmentStoreOp.Store,
                StencilLoadOp = AttachmentLoadOp.DontCare,
                StencilStoreOp = AttachmentStoreOp.DontCare,
                InitialLayout = ImageLayout.Undefined,
                FinalLayout = ImageLayout.ColorAttachmentOptimal,
            };

        AttachmentReference colorAttachmentRef =
            new() { Attachment = 0, Layout = ImageLayout.ColorAttachmentOptimal, };

        AttachmentDescription colorAttachmentResolve =
            new()
            {
                Format = swapChainFormat,
                Samples = SampleCountFlags.Count1Bit,
                LoadOp = AttachmentLoadOp.DontCare,
                StoreOp = AttachmentStoreOp.Store,
                StencilLoadOp = AttachmentLoadOp.DontCare,
                StencilStoreOp = AttachmentStoreOp.DontCare,
                InitialLayout = ImageLayout.Undefined,
                FinalLayout = ImageLayout.PresentSrcKhr
            };

        AttachmentReference colorAttachmentResolveRef =
            new() { Attachment = 2, Layout = ImageLayout.AttachmentOptimalKhr };

        SubpassDescription subpass =
            new()
            {
                PipelineBindPoint = PipelineBindPoint.Graphics,
                ColorAttachmentCount = 1,
                PColorAttachments = &colorAttachmentRef,
                PDepthStencilAttachment = &depthAttachmentRef,
                PResolveAttachments = &colorAttachmentResolveRef
            };

        SubpassDependency dependency =
            new()
            {
                DstSubpass = 0,
                DstAccessMask =
                    AccessFlags.ColorAttachmentWriteBit
                    | AccessFlags.DepthStencilAttachmentWriteBit,
                DstStageMask =
                    PipelineStageFlags.ColorAttachmentOutputBit
                    | PipelineStageFlags.EarlyFragmentTestsBit,
                SrcSubpass = Vk.SubpassExternal,
                SrcAccessMask = 0,
                SrcStageMask =
                    PipelineStageFlags.ColorAttachmentOutputBit
                    | PipelineStageFlags.EarlyFragmentTestsBit,
            };

        var attachments = new[] { colorAttachment, depthAttachment, colorAttachmentResolve };

        fixed (AttachmentDescription* attachmentsPtr = attachments)
        {
            RenderPassCreateInfo renderPassInfo =
                new()
                {
                    SType = StructureType.RenderPassCreateInfo,
                    AttachmentCount = (uint)attachments.Length,
                    PAttachments = attachmentsPtr,
                    SubpassCount = 1,
                    PSubpasses = &subpass,
                    DependencyCount = 1,
                    PDependencies = &dependency,
                };

            var resultVulkan = vk.CreateRenderPass(device, renderPassInfo, null, out renderPass);
            if (resultVulkan != Result.Success)
            {
                throw new VulkanException("Vulkan: failed to create render pass: {0}", resultVulkan);
            }
        }
    }

    private unsafe DeviceMemory Initialize1(
        Vk vk,
        nint pixels,
        ulong uploadSize,
        Silk.NET.Vulkan.Buffer uploadBuffer,
        MemoryRequirements uploadReq
    )
    {
        var uploadAllocInfo = new MemoryAllocateInfo
        {
            SType = StructureType.MemoryAllocateInfo,
            AllocationSize = uploadReq.Size,
            MemoryTypeIndex = GetMemoryTypeIndex(
                vk,
                MemoryPropertyFlags.HostVisibleBit,
                uploadReq.MemoryTypeBits
            )
        };

        var resultVulkan = this.vk.AllocateMemory(device, uploadAllocInfo, default, out var uploadBufferMemory);
        if (resultVulkan != Result.Success)
        {
            throw new VulkanException("Vulkan: failed to allocate device memory: {0}", resultVulkan);
        }
        resultVulkan = this.vk.BindBufferMemory(device, uploadBuffer, uploadBufferMemory, 0);
        if (resultVulkan != Result.Success)
        {
            throw new VulkanException("Vulkan: failed to bind device memory: {0}", resultVulkan);
        }

        void* map = null;

        resultVulkan = vk.MapMemory(device, uploadBufferMemory, 0, uploadSize, 0, &map);
        if (resultVulkan != Result.Success)
        {
            throw new VulkanException("Vulkan: failed to map device memory: {0}", resultVulkan);
        }
        Unsafe.CopyBlock(map, pixels.ToPointer(), (uint)uploadSize);

        var range = new MappedMemoryRange
        {
            SType = StructureType.MappedMemoryRange,
            Memory = uploadBufferMemory,
            Size = uploadSize
        };

        resultVulkan = vk.FlushMappedMemoryRanges(device, 1, range);
        if (resultVulkan != Result.Success)
        {
            throw new VulkanException("Vulkan: failed to flush memory to device: {0}", resultVulkan);
        }
        this.vk.UnmapMemory(device, uploadBufferMemory);
        return uploadBufferMemory;
    }

    private unsafe void Initialize4(
        int width,
        int height,
        CommandBuffer commandBuffer,
        Silk.NET.Vulkan.Buffer uploadBuffer
    )
    {
        const uint vkQueueFamilyIgnored = ~0U;

        var copyBarrier = new ImageMemoryBarrier
        {
            SType = StructureType.ImageMemoryBarrier,
            DstAccessMask = AccessFlags.TransferWriteBit,
            OldLayout = ImageLayout.Undefined,
            NewLayout = ImageLayout.TransferDstOptimal,
            SrcQueueFamilyIndex = vkQueueFamilyIgnored,
            DstQueueFamilyIndex = vkQueueFamilyIgnored,
            Image = fontImage
        };
        copyBarrier.SubresourceRange.AspectMask = ImageAspectFlags.ColorBit;
        copyBarrier.SubresourceRange.LevelCount = 1;
        copyBarrier.SubresourceRange.LayerCount = 1;
        this.vk.CmdPipelineBarrier(
            commandBuffer,
            PipelineStageFlags.HostBit,
            PipelineStageFlags.TransferBit,
            0,
            0,
            default,
            0,
            default,
            1,
            copyBarrier
        );

        var region = new BufferImageCopy();
        region.ImageSubresource.AspectMask = ImageAspectFlags.ColorBit;
        region.ImageSubresource.LayerCount = 1;
        region.ImageExtent.Width = (uint)width;
        region.ImageExtent.Height = (uint)height;
        region.ImageExtent.Depth = 1;
        this.vk.CmdCopyBufferToImage(
            commandBuffer,
            uploadBuffer,
            fontImage,
            ImageLayout.TransferDstOptimal,
            1,
            &region
        );

        var useBarrier = new ImageMemoryBarrier
        {
            SType = StructureType.ImageMemoryBarrier,
            SrcAccessMask = AccessFlags.TransferWriteBit,
            DstAccessMask = AccessFlags.ShaderReadBit,
            OldLayout = ImageLayout.TransferDstOptimal,
            NewLayout = ImageLayout.ShaderReadOnlyOptimal,
            SrcQueueFamilyIndex = vkQueueFamilyIgnored,
            DstQueueFamilyIndex = vkQueueFamilyIgnored,
            Image = fontImage
        };
        useBarrier.SubresourceRange.AspectMask = ImageAspectFlags.ColorBit;
        useBarrier.SubresourceRange.LevelCount = 1;
        useBarrier.SubresourceRange.LayerCount = 1;
        this.vk.CmdPipelineBarrier(
            commandBuffer,
            PipelineStageFlags.TransferBit,
            PipelineStageFlags.FragmentShaderBit,
            0,
            0,
            default,
            0,
            default,
            1,
            useBarrier
        );
    }

    private uint GetMemoryTypeIndex(Vk vk, MemoryPropertyFlags properties, uint typeBits)
    {
        vk.GetPhysicalDeviceMemoryProperties(physicalDevice, out var prop);
        for (var i = 0; i < prop.MemoryTypeCount; i++)
        {
            if (
                (prop.MemoryTypes[i].PropertyFlags & properties) == properties
                && (typeBits & (1u << i)) != 0
            )
            {
                return (uint)i;
            }
        }
        return 0xFFFFFFFF; // Unable to find memoryType
    }

    private void BeginFrame()
    {
        ImGuiNET.ImGui.NewFrame();
        frameBegun = true;
        keyboard = input.Keyboards[0];
        view.Resize += WindowResized;
    }

    private void WindowResized(Vector2D<int> size)
    {
        windowWidth = size.X;
        windowHeight = size.Y;
    }

    private void SetPerFrameImGuiData(float deltaSeconds)
    {
        // var io = ImGuiNET.ImGui.GetIO();
        io.DisplaySize = new Vector2(windowWidth, windowHeight);

        if (windowWidth > 0 && windowHeight > 0)
        {
            io.DisplayFramebufferScale = new Vector2(
                view.FramebufferSize.X / windowWidth,
                view.FramebufferSize.Y / windowHeight
            );
        }

        io.DeltaTime = deltaSeconds; // DeltaTime is in seconds.
    }

    private void UpdateImGuiInput()
    {
        // var io = ImGuiNET.ImGui.GetIO();

        var mouseState = input.Mice[0].CaptureState();
        var keyboardState = input.Keyboards[0];

        io.MouseDown[0] = mouseState.IsButtonPressed(MouseButton.Left);
        io.MouseDown[1] = mouseState.IsButtonPressed(MouseButton.Right);
        io.MouseDown[2] = mouseState.IsButtonPressed(MouseButton.Middle);

        var point = new Point((int)mouseState.Position.X, (int)mouseState.Position.Y);
        io.MousePos = new Vector2(point.X, point.Y);

        var wheel = mouseState.GetScrollWheels()[0];
        io.MouseWheel = wheel.Y;
        io.MouseWheelH = wheel.X;

        foreach (Key key in Enum.GetValues(typeof(Key)))
        {
            if (key == Key.Unknown)
            {
                continue;
            }
            // io.KeysDown[(int)key] = keyboardState.IsKeyPressed(key);
        }

        foreach (var c in pressedChars)
        {
            io.AddInputCharacter(c);
        }

        pressedChars.Clear();

        io.KeyCtrl =
            keyboardState.IsKeyPressed(Key.ControlLeft)
            || keyboardState.IsKeyPressed(Key.ControlRight);
        io.KeyAlt =
            keyboardState.IsKeyPressed(Key.AltLeft) || keyboardState.IsKeyPressed(Key.AltRight);
        io.KeyShift =
            keyboardState.IsKeyPressed(Key.ShiftLeft) || keyboardState.IsKeyPressed(Key.ShiftRight);
        io.KeySuper =
            keyboardState.IsKeyPressed(Key.SuperLeft) || keyboardState.IsKeyPressed(Key.SuperRight);
    }

    internal void PressChar(char keyChar)
    {
        pressedChars.Add(keyChar);
    }

    private static void SetKeyMappings()
    {
        // var io = ImGuiNET.ImGui.GetIO();
        // io.KeyMap[(int)ImGuiKey.Tab] = (int)Key.Tab;
        // io.KeyMap[(int)ImGuiKey.LeftArrow] = (int)Key.Left;
        // io.KeyMap[(int)ImGuiKey.RightArrow] = (int)Key.Right;
        // io.KeyMap[(int)ImGuiKey.UpArrow] = (int)Key.Up;
        // io.KeyMap[(int)ImGuiKey.DownArrow] = (int)Key.Down;
        // io.KeyMap[(int)ImGuiKey.PageUp] = (int)Key.PageUp;
        // io.KeyMap[(int)ImGuiKey.PageDown] = (int)Key.PageDown;
        // io.KeyMap[(int)ImGuiKey.Home] = (int)Key.Home;
        // io.KeyMap[(int)ImGuiKey.End] = (int)Key.End;
        // io.KeyMap[(int)ImGuiKey.Delete] = (int)Key.Delete;
        // io.KeyMap[(int)ImGuiKey.Backspace] = (int)Key.Backspace;
        // io.KeyMap[(int)ImGuiKey.Enter] = (int)Key.Enter;
        // io.KeyMap[(int)ImGuiKey.Escape] = (int)Key.Escape;
        // io.KeyMap[(int)ImGuiKey.A] = (int)Key.A;
        // io.KeyMap[(int)ImGuiKey.C] = (int)Key.C;
        // io.KeyMap[(int)ImGuiKey.V] = (int)Key.V;
        // io.KeyMap[(int)ImGuiKey.X] = (int)Key.X;
        // io.KeyMap[(int)ImGuiKey.Y] = (int)Key.Y;
        // io.KeyMap[(int)ImGuiKey.Z] = (int)Key.Z;
    }

    private unsafe void RenderImDrawData(
        in ImDrawDataPtr drawDataPtr,
        in CommandBuffer commandBuffer,
        in Framebuffer framebuffer,
        in Extent2D swapChainExtent
    )
    {
        var framebufferWidth = (int)(drawDataPtr.DisplaySize.X * drawDataPtr.FramebufferScale.X);
        var framebufferHeight = (int)(drawDataPtr.DisplaySize.Y * drawDataPtr.FramebufferScale.Y);
        if (framebufferWidth <= 0 || framebufferHeight <= 0)
        {
            return;
        }

        var drawData = *drawDataPtr.NativePtr;

        // Avoid rendering when minimized, scale coordinates for retina displays (screen coordinates != framebuffer coordinates)
        var fbWidth = (int)(drawData.DisplaySize.X * drawData.FramebufferScale.X);
        var fbHeight = (int)(drawData.DisplaySize.Y * drawData.FramebufferScale.Y);
        if (fbWidth <= 0 || fbHeight <= 0)
        {
            return;
        }

        // Allocate array to store enough vertex/index buffers
        var frameRenderBuffer = RenderCreateBuffer();

        if (drawData.TotalVtxCount > 0)
        {
            frameRenderBuffer = RenderCreateOrResizeBuffer(drawData, ref frameRenderBuffer);

            // Upload vertex/index data into a single contiguous GPU buffer
            ImDrawVert* vtxDst = null;
            ushort* idxDst = null;

            var resultVulkan = vk.MapMemory(device, frameRenderBuffer.VertexBufferMemory, 0, frameRenderBuffer.VertexBufferSize, 0, (void**)&vtxDst);
            if (resultVulkan != Result.Success)
            {
                throw new VulkanException("Vulkan: failed to map device memory: {0}", resultVulkan);
            }

            resultVulkan = vk.MapMemory(device, frameRenderBuffer.IndexBufferMemory, 0, frameRenderBuffer.IndexBufferSize, 0, (void**)&idxDst);
            if (resultVulkan != Result.Success)
            {
                throw new VulkanException("Vulkan: failed to map device memory: {0}", resultVulkan);
            }

            for (var n = 0; n < drawData.CmdListsCount; n++)
            {
                var cmdList = drawData.CmdLists.Ref<ImDrawListPtr>(n);
                Unsafe.CopyBlock(
                    vtxDst,
                    cmdList.VtxBuffer.Data.ToPointer(),
                    (uint)cmdList.VtxBuffer.Size * (uint)sizeof(ImDrawVert)
                );
                Unsafe.CopyBlock(
                    idxDst,
                    cmdList.IdxBuffer.Data.ToPointer(),
                    (uint)cmdList.IdxBuffer.Size * sizeof(ushort)
                );
                vtxDst += cmdList.VtxBuffer.Size;
                idxDst += cmdList.IdxBuffer.Size;
            }

            RenderFlushMappedMemoryRanges(frameRenderBuffer);
            vk.UnmapMemory(device, frameRenderBuffer.VertexBufferMemory);
            vk.UnmapMemory(device, frameRenderBuffer.IndexBufferMemory);
        }

        // Setup desired Vulkan state
        vk.CmdBindPipeline(commandBuffer, PipelineBindPoint.Graphics, pipeline);
        vk.CmdBindDescriptorSets(
            commandBuffer,
            PipelineBindPoint.Graphics,
            pipelineLayout,
            0,
            1,
            descriptorSet,
            0,
            null
        );

        // Bind Vertex And Index Buffer:
        if (drawData.TotalVtxCount > 0)
        {
            ReadOnlySpan<Silk.NET.Vulkan.Buffer> vertexBuffers =
                [frameRenderBuffer.VertexBuffer];
            ulong vertexOffset = 0;
            vk.CmdBindVertexBuffers(
                commandBuffer,
                0,
                1,
                vertexBuffers,
                (ulong*)Unsafe.AsPointer(ref vertexOffset)
            );
            vk.CmdBindIndexBuffer(
                commandBuffer,
                frameRenderBuffer.IndexBuffer,
                0,
                sizeof(ushort) == 2 ? IndexType.Uint16 : IndexType.Uint32
            );
        }

        // Setup viewport:
        RenderSetupViewport(commandBuffer, drawData, fbWidth, fbHeight);

        // Render command lists
        // (Because we merged all buffers into a single one, we maintain our own offset into them)
        RenderCommandLists(commandBuffer, drawData, fbWidth, fbHeight);
    }

    private unsafe FrameRenderBuffer RenderCreateOrResizeBuffer(
        ImDrawData drawData,
        ref FrameRenderBuffer frameRenderBuffer
    )
    {
        // Create or resize the vertex/index buffers
        var vertexSize = (ulong)drawData.TotalVtxCount * (ulong)sizeof(ImDrawVert);
        var indexSize = (ulong)drawData.TotalIdxCount * sizeof(ushort);
        if (
            frameRenderBuffer.VertexBuffer.Handle == default
            || frameRenderBuffer.VertexBufferSize < vertexSize
        )
        {
            CreateOrResizeBuffer(
                ref frameRenderBuffer.VertexBuffer,
                ref frameRenderBuffer.VertexBufferMemory,
                ref frameRenderBuffer.VertexBufferSize,
                vertexSize,
                BufferUsageFlags.VertexBufferBit
            );
        }
        if (
            frameRenderBuffer.IndexBuffer.Handle == default
            || frameRenderBuffer.IndexBufferSize < indexSize
        )
        {
            CreateOrResizeBuffer(
                ref frameRenderBuffer.IndexBuffer,
                ref frameRenderBuffer.IndexBufferMemory,
                ref frameRenderBuffer.IndexBufferSize,
                indexSize,
                BufferUsageFlags.IndexBufferBit
            );
        }

        return frameRenderBuffer;
    }

    private unsafe void RenderFlushMappedMemoryRanges(FrameRenderBuffer frameRenderBuffer)
    {
        Span<MappedMemoryRange> range = stackalloc MappedMemoryRange[2];
        range[0].SType = StructureType.MappedMemoryRange;
        range[0].Memory = frameRenderBuffer.VertexBufferMemory;
        range[0].Size = Vk.WholeSize;
        range[1].SType = StructureType.MappedMemoryRange;
        range[1].Memory = frameRenderBuffer.IndexBufferMemory;
        range[1].Size = Vk.WholeSize;

        var resultVulkan = vk.FlushMappedMemoryRanges(device, 2, range);
        if (resultVulkan != Result.Success)
        {
            throw new VulkanException("Vulkan: failed to flush memory to device: {0}", resultVulkan);
        }
    }

    private unsafe FrameRenderBuffer RenderCreateBuffer()
    {
        if (mainWindowRenderBuffers.FrameRenderBuffers == null)
        {
            mainWindowRenderBuffers.Index = 0;
            mainWindowRenderBuffers.Count = (uint)swapChainImageCt;
            frameRenderBuffers = GlobalMemory.Allocate(
                sizeof(FrameRenderBuffer) * (int)mainWindowRenderBuffers.Count
            );
            mainWindowRenderBuffers.FrameRenderBuffers =
                frameRenderBuffers.AsPtr<FrameRenderBuffer>();
            for (var i = 0; i < (int)mainWindowRenderBuffers.Count; i++)
            {
                mainWindowRenderBuffers.FrameRenderBuffers[i].IndexBuffer.Handle = 0;
                mainWindowRenderBuffers.FrameRenderBuffers[i].IndexBufferSize = 0;
                mainWindowRenderBuffers.FrameRenderBuffers[i].IndexBufferMemory.Handle = 0;
                mainWindowRenderBuffers.FrameRenderBuffers[i].VertexBuffer.Handle = 0;
                mainWindowRenderBuffers.FrameRenderBuffers[i].VertexBufferSize = 0;
                mainWindowRenderBuffers.FrameRenderBuffers[i].VertexBufferMemory.Handle = 0;
            }
        }
        mainWindowRenderBuffers.Index =
            (mainWindowRenderBuffers.Index + 1) % mainWindowRenderBuffers.Count;

        ref var frameRenderBuffer = ref mainWindowRenderBuffers.FrameRenderBuffers[
            mainWindowRenderBuffers.Index
        ];
        return frameRenderBuffer;
    }

    private unsafe void RenderSetupViewport(
        CommandBuffer commandBuffer,
        ImDrawData drawData,
        int fbWidth,
        int fbHeight
    )
    {
        Viewport viewport;
        viewport.X = 0;
        viewport.Y = 0;
        viewport.Width = fbWidth;
        viewport.Height = fbHeight;
        viewport.MinDepth = 0.0f;
        viewport.MaxDepth = 1.0f;
        vk.CmdSetViewport(commandBuffer, 0, 1, &viewport);

        // Setup scale and position:
        // Our visible imgui space lies from draw_data.DisplayPps (top left) to draw_data.DisplayPos+data_data.DisplaySize (bottom right). DisplayPos is (0,0) for single viewport apps.
        Span<float> scale = [2.0f / drawData.DisplaySize.X, 2.0f / drawData.DisplaySize.Y];
        Span<float> translate = [-1.0f - (drawData.DisplayPos.X * scale[0]), -1.0f - (drawData.DisplayPos.Y * scale[1])];
        vk.CmdPushConstants(
            commandBuffer,
            pipelineLayout,
            ShaderStageFlags.VertexBit,
            sizeof(float) * 0,
            sizeof(float) * 2,
            scale
        );
        vk.CmdPushConstants(
            commandBuffer,
            pipelineLayout,
            ShaderStageFlags.VertexBit,
            sizeof(float) * 2,
            sizeof(float) * 2,
            translate
        );
    }

    private unsafe void RenderCommandLists(
        CommandBuffer commandBuffer,
        ImDrawData drawData,
        int fbWidth,
        int fbHeight
    )
    {
        // Will project scissor/clipping rectangles into framebuffer space
        var clipOff = drawData.DisplayPos; // (0,0) unless using multi-viewports
        var clipScale = drawData.FramebufferScale; // (1,1) unless using retina display which are often (2,2)

        var vertexOffset = 0;
        var indexOffset = 0;
        for (var n = 0; n < drawData.CmdListsCount; n++)
        {
            var cmdList = drawData.CmdLists.Ref<ImDrawListPtr>(n);
            for (var cmdI = 0; cmdI < cmdList.CmdBuffer.Size; cmdI++)
            {
                var pcmd = cmdList.CmdBuffer[cmdI];

                // Project scissor/clipping rectangles into framebuffer space
                Vector4D<float> clipRect;
                clipRect.X = (pcmd.ClipRect.X - clipOff.X) * clipScale.X;
                clipRect.Y = (pcmd.ClipRect.Y - clipOff.Y) * clipScale.Y;
                clipRect.Z = (pcmd.ClipRect.Z - clipOff.X) * clipScale.X;
                clipRect.W = (pcmd.ClipRect.W - clipOff.Y) * clipScale.Y;

                if (
                    clipRect.X < fbWidth
                    && clipRect.Y < fbHeight
                    && clipRect.Z >= 0.0f
                    && clipRect.W >= 0.0f
                )
                {
                    // Negative offsets are illegal for vkCmdSetScissor
                    if (clipRect.X < 0.0f)
                    {
                        clipRect.X = 0.0f;
                    }

                    if (clipRect.Y < 0.0f)
                    {
                        clipRect.Y = 0.0f;
                    }

                    // Apply scissor/clipping rectangle
                    var scissor = new Rect2D();
                    scissor.Offset.X = (int)clipRect.X;
                    scissor.Offset.Y = (int)clipRect.Y;
                    scissor.Extent.Width = (uint)(clipRect.Z - clipRect.X);
                    scissor.Extent.Height = (uint)(clipRect.W - clipRect.Y);
                    vk.CmdSetScissor(commandBuffer, 0, 1, &scissor);

                    // Draw
                    vk.CmdDrawIndexed(
                        commandBuffer,
                        pcmd.ElemCount,
                        1,
                        pcmd.IdxOffset + (uint)indexOffset,
                        (int)pcmd.VtxOffset + vertexOffset,
                        0
                    );
                }
            }
            indexOffset += cmdList.IdxBuffer.Size;
            vertexOffset += cmdList.VtxBuffer.Size;
        }
    }

    private unsafe void CreateOrResizeBuffer(
        ref Silk.NET.Vulkan.Buffer buffer,
        ref DeviceMemory bufferMemory,
        ref ulong bufferSize,
        ulong newSize,
        BufferUsageFlags usage
    )
    {
        if (buffer.Handle != default)
        {
            vk.DestroyBuffer(device, buffer, default);
        }
        if (bufferMemory.Handle != default)
        {
            vk.FreeMemory(device, bufferMemory, default);
        }

        var sizeAlignedVertexBuffer =
            (((newSize - 1) / bufferMemoryAlignment) + 1) * bufferMemoryAlignment;
        var bufferInfo = new BufferCreateInfo
        {
            SType = StructureType.BufferCreateInfo,
            Size = sizeAlignedVertexBuffer,
            Usage = usage,
            SharingMode = SharingMode.Exclusive
        };

        var resultVulkan = vk.CreateBuffer(device, bufferInfo, default, out buffer);
        if (resultVulkan != Result.Success)
        {
            throw new VulkanException("Vulkan: failed to create a device buffer: {0}", resultVulkan);
        }

        vk.GetBufferMemoryRequirements(device, buffer, out var req);
        bufferMemoryAlignment =
            (bufferMemoryAlignment > req.Alignment) ? bufferMemoryAlignment : req.Alignment;
        var allocInfo = new MemoryAllocateInfo
        {
            SType = StructureType.MemoryAllocateInfo,
            AllocationSize = req.Size,
            MemoryTypeIndex = GetMemoryTypeIndex(
                vk,
                MemoryPropertyFlags.HostVisibleBit,
                req.MemoryTypeBits
            )
        };

        resultVulkan = this.vk.AllocateMemory(device, &allocInfo, default, out bufferMemory);
        if (resultVulkan != Result.Success)
        {
            throw new VulkanException("Vulkan: failed to allocate device memory: {0}", resultVulkan);
        }
        resultVulkan = this.vk.BindBufferMemory(device, buffer, bufferMemory, 0);
        if (resultVulkan != Result.Success)
        {
            throw new VulkanException("Vulkan: failed to bind device memory: {0}", resultVulkan);
        }
        bufferSize = req.Size;
    }
}
