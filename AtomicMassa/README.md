# Main source code

> ***
> This Directory: The Heart and Soul Behind AtomicMassa
> ***

This folder houses the groundbreaking source code behind **AtomicMassa**. It's meticulously divided into the following vital parts:

- **[Engine](./Engine)**: The underlying technology making the magic happen.
- **[Editor](./Editor)**: The sleek and powerful GUI, assisting you in game and app development.
- **[Platforms](./Platforms)**: The glue to port and ship your game to different platforms.

## Exploration

Navigate through each directory to uncover the secrets behind **AtomicMassa**. Witness the magic and the mind behind the engine that is changing the game development landscape.

Feel free to explore, learn, and even contribute. After all, great knowledge lies within the heart of **AtomicMassa**, and it's all here in the source code.
