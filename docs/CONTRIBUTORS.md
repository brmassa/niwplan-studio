# Contributors

Now, let's introduce you to the legendary masterminds who've made this project a true masterpiece. Feast your eyes on the list of these amazing contributors, who've generously shared their time, skills, and passion with us! 🌟🧙‍♂️🎊

> IMPORTANT: the following list will be parsed and included into the code.

* [Bruno Massa](https://gitlab.com/brmassa)
