# Building from source

Are you confident that you can build **AtomicMassa** from source or program C# to help it?

You'll need the .NET SDK 7 installed on your machine. Check  Once you have that installed, simply clone this repository and use your favorite IDE to build the project. Then, launch the executable and start creating your next masterpiece.

If you run into any issues during the installation or build process, don't hesitate to reach out to our support team for assistance. We're here to help you get started with **AtomicMassa** and create amazing 3D games that will impress your players.

## Nuke Build system

In order to replicate the same building steps whenever it's in your local pc, in the cloud or anywhere in between, we use the [Nuke](https://nuke.build/) system to abstract.

### The build command

* On Linux: `build.sh`
* On Windows: `build.ps1`
* If your have Nuke installed (optional): `nuke`

### Build

It will build the all the projects for you.

On Linux:

```bash
./build.sh compile
```

On Windows:

```powershell
./build.ps1 compile
```

For subsequent builds, we suggest to use "--skip-shaders true" to skip the step of rebuilding shaders.

### Test

On Linux:

```bash
./build.sh test
```

On Windows:

```powershell
./build.ps1 test
```

### Other commands

We configured the Nuke system to perform other tasks too:

* Publish: to create the "release" version of the code
* Test-Report: print a small html local site with the results of the testing coverage.
* Compile-Shaders: compile the needed shaders
