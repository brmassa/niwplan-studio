# Participate

Greetings, fellow adventurer! We're absolutely thrilled you've decided to join us in our quest to create the most _epic_, super open source engine the world has ever seen! This guide will take you on a journey through the various ways you can contribute to this magical endeavor, both directly and indirectly. So buckle up, and let's get started!

## 📚 How?

1. Direct
    - [🐞 Reporting bugs](#reporting-bugs)
    - [💡 Proposing features or improvements](#proposing-features-or-improvements)
    - [🌟 Program](#program)
    - [🌍 Translation](#translation)
2. Indirect Contributions
    - [📚 Creating tutorials](#creating-tutorials)
    - [🤝 Helping others](#helping-others)

## Direct

### Reporting bugs

Nobody's perfect, and our engine is no exception. If you happen to stumble upon a pesky bug, don't hesitate to let us know! Just head on over to the [Issues](https://gitlab.com/AtomicMassa/AtomicMassa/-/issues) page and create a new issue with a descriptive title and detailed information about the bug. The more information you provide, the better equipped our bug-busting team will be to squash that bug into oblivion!

### Proposing features or improvements

Have a brilliant idea that'll take our engine to new heights? Don't keep it to yourself! Share your wisdom with us by creating a new issue in the [Issues](https://gitlab.com/AtomicMassa/AtomicMassa/-/issues) page. Make sure to label it as a "feature request" or "enhancement" and provide a detailed description of your vision. Your idea might just be the next big thing!

### Program

Ready to roll up your sleeves and dive into the code? You rock! We welcome your pull requests with open arms (and maybe even a virtual high-five!). Just follow these steps:

- Fork the repository
- Create a new branch (e.g., your-username/your-feature)
- Commit your changes
- Push the branch to your fork
- Open a pull request against the main repository

Remember to write a detailed description of your changes, and be prepared to address any feedback from our team of code connoisseurs.

### Translation

C-3PO may have known over six million forms of communication, but we're not quite there yet. Help us make our engine accessible to users all around the globe by contributing to our translation efforts.

## Indirect Contributions

### Creating tutorials

Knowledge is power,and sharing that power with others is the noblest of pursuits. If you have a knack for explaining complex concepts in a simple, engaging manner, why not create a tutorial to help others learn the ropes of our engine? Whether it's a written guide, a video tutorial, or a live-streamed workshop, your educational content will help others unlock their full potential and contribute to our engine's success.

### Helping others

We're all in this together, and sometimes, even the most seasoned adventurers need a helping hand. If you see someone in our community struggling with a problem or seeking guidance, don't hesitate to lend your expertise. By helping others, you're not only fostering a positive, collaborative atmosphere, but you're also strengthening our engine's foundation one interaction at a time.

> ***
> START NOW
> ***
